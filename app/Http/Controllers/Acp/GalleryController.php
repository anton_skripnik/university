<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Requests\Acp\GalleryRequest;
use App\Models\Gallery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class GalleryController
 *
 * @package App\Http\Controllers\Acp
 */
class GalleryController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return \view('acp.gallery.index', [
            'photos' => Gallery::paginate(16),
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('acp.gallery.create');
    }

    /**
     * @param GalleryRequest $request
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function store(GalleryRequest $request): RedirectResponse
    {
        $photo = Gallery::create([
            'title' => $request->input('title'),
        ]);

        // add photo if any
        if ($request->hasFile('photo') && ! \is_null($request->file('photo'))) {
            $photo->addMediaFromRequest('photo')->toMediaCollection('gallery');
        }

        return \redirect()->route('acp.gallery.index')
            ->with('success', \trans('acp/gallery.create.form.messages.success'));

    }

    /**
     * @param Gallery $photo
     *
     * @return View
     */
    public function edit(Gallery $photo): View
    {
        return \view('acp.gallery.edit', [
            'photo' => $photo,
        ]);
    }

    /**
     * @param GalleryRequest $request
     * @param Gallery        $photo
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function update(GalleryRequest $request, Gallery $photo): RedirectResponse
    {
        $photo->update([
            'title' => $request->input('title'),
        ]);

        // update photo if any
        if ($request->hasFile('photo') && ! \is_null($request->file('photo'))) {
            $photo->clearMediaCollection('gallery');
            $photo->addMediaFromRequest('photo')->toMediaCollection('gallery');
        }
        return \redirect()->route('acp.gallery.index')
            ->with('success', \trans('acp/gallery.edit.form.messages.success'));
    }

    /**
     * @param Gallery $photo
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(Gallery $photo): JsonResponse
    {
        // clear media
        $photo->clearMediaCollection('gallery');

        // resutn json response
        return (false === $photo->delete())
            ? response()->json(\trans('acp/gallery.delete.messages.failure'))
            : response()->json(\trans('acp/gallery.delete.messages.success'));
    }
}
