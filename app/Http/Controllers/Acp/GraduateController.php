<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Requests\Acp\GraduatePositionRequest;
use App\Http\Requests\Acp\GraduateRequest;
use App\Models\Graduate;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class GraduateController
 *
 * @package App\Http\Controllers\Acp
 */
class GraduateController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return \view('acp.graduates.index', [
            'graduates' => Graduate::get(),
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('acp.graduates.create');
    }

    /**
     * @param GraduateRequest $request
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function store(GraduateRequest $request): RedirectResponse
    {
        $graduate = Graduate::create([
            'name'      => $request->input('name'),
            'subheader' => $request->input('subheader'),
            'text'      => $request->input('text'),
        ]);

        // add photo if any
        if ($request->hasFile('avatar') && !\is_null($request->file('avatar'))) {
            $graduate->addMediaFromRequest('avatar')->toMediaCollection('graduates');
        }

        return \redirect()->route('acp.graduate.index')
            ->with('success', \trans('acp/graduate.create.form.messages.success'));
    }

    /**
     * @param Graduate $graduate
     *
     * @return View
     */
    public function show(Graduate $graduate): View
    {
        return \view('acp.graduates.single', [
            'graduate' => $graduate,
        ]);
    }

    /**
     * @param Graduate $graduate
     *
     * @return View
     */
    public function edit(Graduate $graduate): View
    {
        return \view('acp.graduates.edit', [
            'graduate' => $graduate,
        ]);
    }

    /**
     * @param GraduateRequest $request
     * @param Graduate        $graduate
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function update(GraduateRequest $request, Graduate $graduate): RedirectResponse
    {
        $graduate->update([
            'name'      => $request->input('name'),
            'subheader' => $request->input('subheader'),
            'text'      => $request->input('text'),
        ]);

        // udate photo if any
        if ($request->hasFile('avatar') && !\is_null($request->file('avatar'))) {
            $graduate->clearMediaCollection('graduates');
            $graduate->addMediaFromRequest('avatar')->toMediaCollection('graduates');
        }

        return \redirect()->route('acp.graduate.index')
            ->with('success', \trans('acp/graduate.edit.form.messages.success'));
    }

    /**
     * @param GraduatePositionRequest $request
     *
     * @return JsonResponse
     */
    public function updatePosition(GraduatePositionRequest $request): JsonResponse
    {
        foreach ($request->data as $position => $id) {
            Graduate::find($id)->update([
                'position' => (int)$position,
            ]);
        }

        return \response()->json(\trans('acp/graduate.edit.form.messages.success'));
    }

    /**
     * @param Graduate $graduate
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(Graduate $graduate): JsonResponse
    {
        // clear media
        $graduate->clearMediaCollection('graduates');

        // resutn json response
        return (false === $graduate->delete())
            ? response()->json(\trans('acp/graduate.delete.messages.failure'))
            : response()->json(\trans('acp/graduate.delete.messages.success'));
    }
}
