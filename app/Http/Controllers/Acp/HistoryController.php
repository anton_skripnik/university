<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Requests\Acp\HistoryRequest;
use App\Models\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\MediaLibrary\Media;

/**
 * Class HistoryController
 *
 * @package App\Http\Controllers\Acp
 */
class HistoryController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return \view('acp.history.index', [
            'history' => History::first(),
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('acp.history.create', [
            'history' => History::first(),
        ]);
    }

    /**
     * @param HistoryRequest $request
     *
     * @return RedirectResponse
     */
    public function store(HistoryRequest $request): RedirectResponse
    {
        $history = History::create($request->only('title', 'description'));

        // add photo if any
        if ($request->hasFile('picture') && !\is_null($request->file('picture'))) {
            $history->addAllMediaFromRequest('picture')->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('aboutus');
            });
        };

        return \redirect()->route('acp.history.index')
            ->with('success', \trans('acp/history.create.form.messages.success'));

    }

    /**
     * @param History $history
     *
     * @return View
     */
    public function edit(History $history):View
    {
        return \view('acp.history.edit', [
            'history' => $history,
        ]);
    }

    /**
     * @param HistoryRequest $request
     * @param History        $history
     *
     * @return RedirectResponse
     */
    public function update(HistoryRequest $request, History $history): RedirectResponse
    {
        $history->update($request->only('title', 'description'));

        // add photo if any
        if ($request->hasFile('picture') && !\is_null($request->file('picture'))) {
            $history->addAllMediaFromRequest('picture')->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('aboutus');
            });
        };
        return \redirect()->route('acp.history.index')
            ->with('success', \trans('acp/history.edit.form.messages.success'));
    }

    /**
     * @param Media $media
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function imageDestroy(Media $media): JsonResponse
    {
        return (false === $media->delete())
            ? response()->json(\trans('acp/news.delete.messages.failure'))
            : response()->json(\trans('acp/news.delete.messages.success'));
    }
}
