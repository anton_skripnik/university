<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp\Link;

use App\Http\Requests\Acp\LinkRequest;
use App\Models\Link\LinkCategory;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class CategoryLinkController
 *
 * @package App\Http\Controllers\Acp\Link
 */
class CategoryLinkController extends Controller
{
    /**
     * @return View
     */
    public function show(): View
    {
        return \view(
            'acp.links.categories',
            [
                'categories' => LinkCategory::paginate(),
            ]
        );
    }

    /**
     * @param LinkRequest  $request
     * @param LinkCategory $category
     *
     * @return JsonResponse
     */
    public function update(LinkRequest $request, LinkCategory $category): JsonResponse
    {

        $category->update([
            'category' => $request->get('category'),
            'icon'     => $request->get('icon'),
        ]);

        return \response()->json(\trans('acp/subject.edit.form.messages.success'));
    }

    /**
     * @param LinkCategory $category
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(LinkCategory $category): JsonResponse
    {
        if ($category->links->isNotEmpty()) {
            return response()->json(
                [
                    'data'   => \trans('acp/link.delete.messages.not_empty'),
                    'status' => 500,
                ]
            );
        } else {
            return (false === $category->delete())
                ? response()->json(\trans('acp/link.delete.messages.failure'))
                : response()->json(\trans('acp/link.delete.messages.success'));
        }
    }
}
