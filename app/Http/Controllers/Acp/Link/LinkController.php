<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp\Link;

use App\Http\Requests\Acp\LinkRequest;
use App\Models\Link\Link;
use App\Http\Controllers\Controller;
use App\Models\Link\LinkCategory;
use Config;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Lang;
use Spatie\MediaLibrary\Media;

/**
 * Class LinkController
 *
 * @package App\Http\Controllers\Acp\Link
 */
class LinkController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return \view('acp.links.index', [
            'links' => Link::paginate(),
        ]);
    }

    /**
     * @param Link $link
     *
     * @return View
     */
    public function show(Link $link): View
    {
        return \view('acp.links.single', [
            'item' => $link,
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('acp.links.create', [
            'categories' => LinkCategory::pluck('category'),
        ]);
    }

    /**
     * @param LinkRequest $request
     *
     * @return RedirectResponse
     */
    public function store(LinkRequest $request): RedirectResponse
    {
        $category = LinkCategory::where('category', $request->input('category'))->first();

        if (\blank($category)) {
            $category = LinkCategory::create([
                'category' => $request->input('category'),
                'icon'     => Config::get('category_icon.icon'),
            ]);
        }

        $link = $category->links()->create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'category_id' => $category->getKey(),
        ]);

        // add photo if any
        if ($files = $request->file('info')) {
            foreach ($request->file('info') as $file) {
                if (\strpos($file->getMimeType(), 'image') === false) {
                    $link->addMedia($file)->toMediaCollection('info_file');
                } else {
                    $link->addMedia($file)->toMediaCollection('info_pic');
                }
            }
        };

        return \redirect()->route('acp.link.index')
            ->with('success', \trans('acp/link.create.form.messages.success'));
    }

    /**
     * @param Link $link
     *
     * @return View
     */
    public function edit(Link $link): View
    {
        return \view('acp.links.edit', [
            'item'       => $link,
            'categories' => LinkCategory::pluck('category', 'id'),
        ]);
    }

    /**
     * @param LinkRequest $request
     * @param Link        $link
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function update(LinkRequest $request, Link $link): RedirectResponse
    {
        $category = LinkCategory::where('category', $request->input('category'))->first();
        if (\blank($category)) {
            $category = LinkCategory::create([
                'category' => $request->input('category'),
                'icon'     => Lang::get('acp/link.icon.default'),
            ]);
        }

        $link->update([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'category_id' => $category->getKey(),
        ]);

        // add photo if any
        if ($files = $request->file('info')) {
            foreach ($request->file('info') as $file) {
                if (\strpos($file->getMimeType(), 'image') === false) {
                    $link->addMedia($file)->toMediaCollection('info_file');
                } else {
                    $link->addMedia($file)->toMediaCollection('info_pic');
                }
            }
        };

        return \redirect()->route('acp.link.index')
            ->with('success', \trans('acp/link.create.form.messages.success'));
    }

    /**
     * @param Link $link
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(Link $link): JsonResponse
    {
        return (false === $link->delete())
            ? response()->json(\trans('acp/link.delete.messages.failure'))
            : response()->json(\trans('acp/link.delete.messages.success'));
    }

    /**
     * @param Media $media
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function imageDestroy(Media $media): JsonResponse
    {
        return (false === $media->delete())
            ? response()->json(\trans('acp/news.delete.messages.failure'))
            : response()->json(\trans('acp/news.delete.messages.success'));
    }
}
