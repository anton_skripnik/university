<?php

namespace App\Http\Controllers\Acp;

use App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Models\NewcomersContactInfo;
use \App\Http\Requests\Acp\NewcomersContactInfoRequest;

class NewcomersContactInfoController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \view('acp.newcomerscontactinfo.index', [
            'newcomerscontactinfo' => NewcomersContactInfo::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\NewcomersContactInfoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Acp\NewcomersContactInfoRequest $request)
    {
        $email = $request->email;
        $cell = $request->cell;
        $school = $request->school;

        if (NewcomersContactInfo::where('email', $email)->where('cell', $cell)->where('school', $school)->exists()) {
          // Do not duplicate records with the same data.
          return;
        }

        $contactInfo = new NewcomersContactInfo;
        $contactInfo->email = $email;
        $contactInfo->cell = $cell;
        $contactInfo->school = $school;

        $contactInfo->save();
    }
}
