<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Controllers\Traits\SlugTrait;
use App\Http\Requests\Acp\NewsRequest;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Spatie\MediaLibrary\Media;

/**
 * Class NewsController
 *
 * @package App\Http\Controllers\Acp
 */
class NewsController extends Controller
{
    use SlugTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('acp.news.index', [
            'news' => News::paginate(10),
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('acp.news.create');
    }

    /**
     * @param NewsRequest $request
     *
     * @return RedirectResponse
     */
    public function store(NewsRequest $request): RedirectResponse
    {
        $item = News::create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'author'      => $request->input('author'),
            'slug'        => $this->getSlug($request->input('title'), News::query()),
        ]);

        // add photo if any
        if ($request->hasFile('picture') && !\is_null($request->file('picture'))) {
            $item->addAllMediaFromRequest('picture')->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('news');
            });
        };

        return \redirect()->route('acp.news.index')
            ->with('success', \trans('acp/news.create.form.messages.success'));
    }

    /**
     * @param News $item
     *
     * @return View
     */
    public function show(News $item): View
    {
        return \view('acp.news.single', [
            'item' => $item,
        ]);
    }

    /**
     * @param News $item
     *
     * @return View
     */
    public function edit(News $item): View
    {
        return \view('acp.news.edit', [
            'item' => $item,
        ]);
    }

    /**
     * @param NewsRequest $request
     * @param News        $item
     *
     * @return RedirectResponse
     */
    public function update(NewsRequest $request, News $item): RedirectResponse
    {
        $slug = $item->getAttribute('title') !== $request->input('title')
            ? $this->getSlug($request->input('title'), News::query())
            : $item->getAttribute('slug');

        $item->update([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'author'      => $request->input('author'),
            'slug'        => $slug,
        ]);
        // add photo if any
        if ($request->hasFile('picture') && !\is_null($request->file('picture'))) {
            $item->addAllMediaFromRequest('picture')->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('news');
            });
        };

        return \redirect()->route('acp.news.index')
            ->with('success', \trans('acp/news.edit.form.messages.success'));
    }

    /**
     * @param News $item
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(News $item): JsonResponse
    {
        // clear media
        $item->clearMediaCollection('news');

        return (false === $item->delete())
            ? response()->json(\trans('acp/news.delete.messages.failure'))
            : response()->json(\trans('acp/news.delete.messages.success'));
    }

    /**
     * @param Media $media
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function imageDestroy(Media $media): JsonResponse
    {
        return (false === $media->delete())
            ? response()->json(\trans('acp/news.delete.messages.failure'))
            : response()->json(\trans('acp/news.delete.messages.success'));
    }
}
