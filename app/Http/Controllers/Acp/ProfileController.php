<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Requests\Acp\ProfileRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class ProfileController
 *
 * @package App\Http\Controllers\Acp
 */
class ProfileController extends Controller
{
    /**
     * @return View
     */
    public function show(): View
    {
        return \view('acp.profile.index', [
            'user' => \Auth::user(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfileRequest $request
     * @param User           $user
     *
     * @return RedirectResponse
     */
    public function update(ProfileRequest $request, User $user): RedirectResponse
    {
        $user->fill([
            'email' => $request->input('email'),
            'name'  => $request->input('name'),
        ]);

        if ($request->filled('password')) {
            $user->fill([
                'password' => \bcrypt($request->input('password'))
            ]);
        }

        // save user
        $user->save();

        // redirect back to the list
        return \redirect()->route('acp.profile.show')
            ->with('success', \trans('acp/profile.edit.form.messages.success'));
    }
}
