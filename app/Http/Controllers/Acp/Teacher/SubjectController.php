<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp\Teacher;

use App\Http\Requests\Acp\Teacher\Subject\SubjectPositionRequest;
use App\Http\Requests\Acp\Teacher\Subject\SubjectRequest;
use App\Models\Teacher\Subject;
use App\Models\Teacher\Teacher;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class SubjectController
 *
 * @package App\Http\Controllers\Acp\Teacher
 */
class SubjectController extends Controller
{
    /**
     * @param Teacher $teacher
     *
     * @return View
     */
    public function create(Teacher $teacher): View
    {
        return \view('acp.teachers.subjects.create', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @param SubjectRequest $request
     * @param Teacher        $teacher
     *
     * @return RedirectResponse
     */
    public function store(SubjectRequest $request, Teacher $teacher): RedirectResponse
    {
        $subjects = $request->input('subject');

        $subjects = array_map(function($subject) {
            return ['name' => $subject];},
            array_filter($subjects)
        );

        $teacher->subjects()->createMany($subjects);

        return \redirect()->route('acp.teacher.show', $teacher->slug)
            ->with('success', \trans('acp/subject.create.form.messages.success'));
    }

    /**
     * @param SubjectRequest $request
     * @param Subject        $subject
     *
     * @return JsonResponse
     */
    public function update(SubjectRequest $request, Subject $subject): JsonResponse
    {
        $subject->update(['name' => $request->get('name')]);

        return \response()->json(\trans('acp/subject.edit.form.messages.success'));
    }

    /**
     * @param SubjectPositionRequest $request
     *
     * @return JsonResponse
     */
    public function updatePosition(SubjectPositionRequest $request): JsonResponse
    {
        foreach ($request->get('data') as $position => $id) {
            Subject::find($id)->update([
                'position' => (int)$position,
            ]);
        }

        return \response()->json(\trans('acp/subject.edit.form.messages.success'));
    }

    /**
     * @param Subject $subject
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(Subject $subject): JsonResponse
    {
        return (false === $subject->delete())
            ? response()->json(\trans('acp/subject.delete.messages.failure'))
            : response()->json(\trans('acp/subject.delete.messages.success'));
    }
}
