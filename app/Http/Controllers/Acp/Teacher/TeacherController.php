<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp\Teacher;

use App\Http\Requests\Acp\Teacher\TeacherPositionRequest;
use App\Http\Requests\Acp\Teacher\TeacherRequest;
use App\Models\Teacher\Teacher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class TeacherController
 *
 * @package App\Http\Controllers\Acp\Teacher
 */
class TeacherController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return \view('acp.teachers.index', [
            'teachers' => Teacher::orderBy('position')->paginate(10),
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('acp.teachers.create');
    }

    /**
     * @param TeacherRequest $request
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function store(TeacherRequest $request): RedirectResponse
    {
        $teacher = Teacher::create([
            'last_name' => $request->input('last_name'),
            'name'      => $request->input('name'),
            'post'      => $request->input('post'),
            'biography' => $request->input('biography'),
            'email'     => $request->input('email'),
            'schedule'  => $request->input('schedule'),
        ]);

        // add photo if any
        if ($request->hasFile('avatar') && !\is_null($request->file('avatar'))) {
            $teacher->addMediaFromRequest('avatar')->toMediaCollection('teachers');
        }

        return \redirect()->route('acp.teacher.index')
            ->with('success', \trans('acp/teacher.create.form.messages.success'));
    }

    /**
     * @param Teacher $teacher
     *
     * @return View
     */
    public function show(Teacher $teacher): View
    {
        return \view('acp.teachers.single', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @param Teacher $teacher
     *
     * @return View
     */
    public function edit(Teacher $teacher): View
    {
        return \view('acp.teachers.edit', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @param TeacherRequest $request
     * @param Teacher        $teacher
     *
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function update(TeacherRequest $request, Teacher $teacher): RedirectResponse
    {
        $teacher->update([
            'last_name' => $request->input('last_name'),
            'name'      => $request->input('name'),
            'post'      => $request->input('post'),
            'biography' => $request->input('biography'),
            'email'     => $request->input('email'),
            'schedule'  => $request->input('schedule'),
        ]);

        // udate photo if any
        if ($request->hasFile('avatar') && !\is_null($request->file('avatar'))) {
            $teacher->clearMediaCollection('teachers');
            $teacher->addMediaFromRequest('avatar')->toMediaCollection('teachers');
        }

        return \redirect()->route('acp.teacher.index')
            ->with('success', \trans('acp/teacher.edit.form.messages.success'));
    }

    /**
     * @param TeacherPositionRequest $request
     *
     * @return JsonResponse
     */
    public function updatePosition(TeacherPositionRequest $request): JsonResponse
    {
        foreach ($request->data as $position => $id) {
            Teacher::find($id)->update([
                'position' => (int)$position,
            ]);
        }

        return \response()->json(\trans('acp/teacher.edit.form.messages.success'));
    }

    /**
     * @param Teacher $teacher
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(Teacher $teacher): JsonResponse
    {
        // clear media
        $teacher->clearMediaCollection('teachers');

        // resutn json response
        return (false === $teacher->delete())
            ? response()->json(\trans('acp/news.delete.messages.failure'))
            : response()->json(\trans('acp/news.delete.messages.success'));
    }
}
