<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Graduate;
use App\Models\History;
use App\Models\Link\LinkCategory;
use App\Models\News;
use App\Models\Teacher\Teacher;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        return \view('client.pages.index', [
            'history' => History::first(),
        ]);
    }

    /**
     * @param News $item
     *
     * @return View
     */
    public function newsShow(News $item): View
    {
        return \view(
            'client.pages.news_single',
            [
                'item' => $item,
                'news' => News::latest()->where('id', '!=', $item->id)->take(3)->get(),
            ]
        );
    }

    /**
     * @return JsonResponse
     */
    public function newsShowMore(): JsonResponse
    {
        $newsRows = [];

        $news = News::paginate(6);
        $newsPages = [
            'current_page' => (int)$news->currentPage(),
            'total'        => (int)$news->total(),
            'per_page'     => (int)$news->perPage(),
        ];

        foreach ($news as $i => $item) {

            $description = str_limit($item->description, 200, '...');

            $newsRows[$i] = [
                'id'          => $item->id,
                'title'       => $item->title,
                'description' => strip_tags(Markdown::convertToHtml($description)),
                'image'       => $item->getFirstMediaUrl('news', 'md'),
                'slug'        => $item->slug,
            ];
        }

        $data = [$newsRows, $newsPages];

        return \response()->json($data);
    }

    /**
     * @return View
     */
    public function teacherIndex(): View
    {
        return \view('client.pages.teachers', [
            'teachers' => Teacher::all(),
        ]);
    }

    /**
     * @return View
     */
    public function graduateIndex(): View
    {
        return \view('client.pages.graduates', [
            'graduates' => Graduate::all(),
        ]);
    }

    /**
     * @return View
     */
    public function newcomersIndex(): View
    {
	return \view('client.pages.newcomers', [
  	    'graduates' => []
        ]);
    }

    /**
     * @param Teacher $teacher
     *
     * @return View
     */
    public function teacherShow(Teacher $teacher): View
    {
        return \view('client.pages.teacher_single', [
            'teacher' => $teacher,
        ]);
    }


    /**
     * @return View
     */
    public function aboutIndex(): View
    {
        return \view('client.pages.about', [
            'item' => History::first(),
        ]);

    }

    /**
     * @return View
     */
    public function infoIndex(): View
    {
        return \view('client.pages.info',
            [
                'categories' => LinkCategory::all(),
            ]);
    }

    /**
     * @param LinkCategory $linkCategory
     *
     * @return View
     */
    public function infoShow(LinkCategory $linkCategory): View
    {
        return \view('client.pages.info_single',
            [
                'info'     => $linkCategory->links()->paginate(),
                'category' => $linkCategory->getAttribute('category'),
            ]);
    }

    /**
     * @return View
     */
    public function contactIndex(): View
    {
        return \view('client.pages.contacts', [
            'contacts' => Contact::first(),
        ]);
    }

    /**
     * @return View
     *
     */
    public function galleryIndex(): View
    {
        return \view('client.pages.gallery', [
            'gallery' => Gallery::paginate(12),
        ]);
    }
}
