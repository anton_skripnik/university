<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Trait SlugTrait
 *
 * @package App\Http\Controllers\Traits
 */
trait SlugTrait
{
    /**
     * TODO: fix slug creation
     * @param string  $title
     * @param Builder $query
     *
     * @return string
     */
    protected function getSlug(string $title, Builder $query): string
    {
        $title = \str_slug($title, '-');
        $currentNumber = self::extractTitleNumber($title);

        $cleanName = trim(preg_replace('/\s+(\d+)$/', '', $title));
        $pattern = '^'.preg_quote($cleanName).' +[0-9]+$';

//        $lastNumeratedName = $query->where('slug', 'REGEXP', $pattern)
//            ->orderBy(DB::raw('LENGTH(title)'), 'DESC')
//            ->orderBy('slug', 'DESC')
//            ->value('slug') ?? $cleanName;

        $slugExist = $query->where('slug', $title)
            ->orWhere('slug', 'REGEXP', $pattern)
            ->orderBy(DB::raw('LENGTH(title)'), 'DESC')
            ->orderBy('slug', 'DESC')
            ->value('slug');

        if ($slugExist) {
            $currentBiggestNumber = self::extractTitleNumber($slugExist);
            $newBiggestNumber = $currentNumber > $currentBiggestNumber
                ? $currentNumber
                : $currentBiggestNumber + 1;

            return $cleanName.($newBiggestNumber !== 0 ? "-{$newBiggestNumber}" : '');
        }

        return $title;
    }

    /**
     * Extract digit from given string.
     *
     * @param string $title
     *
     * @return int
     */
    protected static function extractTitleNumber(string $title): int
    {
        preg_match('/\s+(\d+)$/', $title, $match);

        return !empty($match[1]) ? (int) $match[1] : 0;
    }
}
