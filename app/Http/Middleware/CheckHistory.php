<?php

declare(strict_types = 1);

namespace App\Http\Middleware;

use App\Models\History;
use Closure;

/**
 * Class CheckHistory
 *
 * @package App\Http\Middleware
 */
class CheckHistory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure                  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $history = History::first();
        if (\blank($history)) {
            return $next($request);
        } else {
            return \redirect()->back();
        }
    }
}
