<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ContactRequest
 *
 * @package App\Http\Requests\Acp
 */
class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|min:4|max:255',
            'phone'   => 'nullable|min:4|max:25',
        ];
    }

    /**
     * Custom error messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'address.required' => \trans('acp/contact.edit.form.fields.address.errors.required'),
            'address.string'   => \trans('acp/contact.edit.form.fields.address.errors.string'),
            'address.min'      => \trans('acp/contact.edit.form.fields.address.errors.min'),
            'address.max'      => \trans('acp/contact.edit.form.fields.address.errors.max'),

            'phone.min'      => \trans('acp/contact.edit.form.fields.phone.errors.min'),
            'phone.max'      => \trans('acp/contact.edit.form.fields.phone.errors.max'),

        ];
    }
}
