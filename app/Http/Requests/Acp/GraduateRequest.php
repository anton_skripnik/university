<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GraduateRequest
 * 
 * @package App\Http\Requests\Acp
 */
class GraduateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|string|min:4|max:64',
            'subheader' => 'required|string|min:4|max:255',
            'text'      => 'required|min:100',
            'avatar'    => 'nullable|image',
        ];
    }
}
