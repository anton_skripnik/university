<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class HistoryRequest
 * 
 * @package App\Http\Requests\Acp
 */
class HistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|string|min:4|max:255',
            'description' => 'required',
            'picture.*'   => 'nullable|image',
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required'       => \trans('acp/history.create.form.fields.title.errors.required'),
            'title.string'         => \trans('acp/history.create.form.fields.title.errors.string'),
            'title.min'            => \trans('acp/history.create.form.fields.title.errors.min'),
            'title.max'            => \trans('acp/history.create.form.fields.title.errors.max'),
            'description.required' => \trans('acp/history.create.form.fields.description.errors.required'),
            'picture.*.image'        => \trans('acp/history.create.form.fields.picture.errors.image'),

        ];
    }
}
