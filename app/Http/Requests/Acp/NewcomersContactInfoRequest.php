<?php

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

class NewcomersContactInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email:rfc',
            'cell' => 'required|regex:/^\+\d{12}$/i',
            'school' => 'required'
        ];
    }

    public function messages() 
    {
        return [
            'email.required' => \trans('acp/newcomerscontactinfo.fields.email.errors.required'),
            'email.email' => \trans('acp/newcomerscontactinfo.fields.email.errors.email'),

            'cell.required' => \trans('acp/newcomerscontactinfo.fields.cell.errors.required'),
            'cell.regex' => \trans('acp/newcomerscontactinfo.fields.cell.errors.regex'),

            'school.required' => \trans('acp/newcomerscontactinfo.fields.school.errors.required')
        ];
    }
}
