<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ProfileRequest
 * 
 * @package App\Http\Requests\Acp
 */
class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $except = \Auth::id();

        return [
            'email'    => 'required|email|unique:users,email,'.$except,
            'name'     => 'required|string|min:4|max:64',
            'password' => 'nullable|confirmed|min:6',
        ];
    }

    /**
     * Custom messages
     * 
     * @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => \trans('acp/profile.edit.form.fields.email.errors.required'),
            'email.email' => \trans('acp/profile.edit.form.fields.email.errors.email'),
            'email.unique' => \trans('acp/profile.edit.form.fields.email.errors.unique'),

            'name.required' => \trans('acp/profile.edit.form.fields.name.errors.required'),
            'name.string' => \trans('acp/profile.edit.form.fields.name.errors.string'),
            'name.min' => \trans('acp/profile.edit.form.fields.name.errors.min'),
            'name.max' => \trans('acp/profile.edit.form.fields.name.errors.max'),

            'password.confirmed' => \trans('acp/profile.edit.form.fields.password.errors.confirmed'),
            'password.min' => \trans('acp/profile.edit.form.fields.password.errors.min'),

        ];
    }
}
