<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp\Teacher\Article;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ArticleRequest
 *
 * @package App\Http\Requests\Acp\Teacher\Article
 */
class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article.*.description' => 'string|nullable|min:4|max:400',
            'article.*.link'    => 'string|nullable|min:4|max:255',
        ];
    }

    /**
     * Custom messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'article.*.description.string' => \trans('acp/article.create.form.fields.article.errors.string'),
            'article.*.description.min'    => \trans('acp/article.create.form.fields.article.errors.min'),
            'article.*.description.max'    => \trans('acp/article.create.form.fields.article.errors.max'),

            'link.*.link.string' => \trans('acp/article.create.form.fields.link.errors.string'),
            'link.*.link.min'    => \trans('acp/article.create.form.fields.link.errors.min'),
            'link.*.link.max'    => \trans('acp/article.create.form.fields.link.errors.max'),
        ];
    }
}
