<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp\Teacher\Subject;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SubjectRequest
 *
 * @package App\Http\Requests\Acp\Teacher\Subject
 */
class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject.*' => 'string|nullable|min:4|max:150',
        ];
    }

    /**
     * Custom messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'subject.*.string' => \trans('acp/subject.create.form.fields.subject.errors.string'),
            'subject.*.min'    => \trans('acp/subject.create.form.fields.subject.errors.min'),
            'subject.*.max'    => \trans('acp/subject.create.form.fields.subject.errors.max'),
        ];
    }
}
