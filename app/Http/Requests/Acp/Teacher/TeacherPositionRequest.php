<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp\Teacher;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TeacherPositionRequest
 *
 * @package App\Http\Requests\Acp\Teacher
 */
class TeacherPositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.*.*' => 'required|numeric|exists:teachers',
        ];
    }
}
