<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp\Teacher;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TeacherRequest
 * 
 * @package App\Http\Requests\Acp\Teacher
 */
class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required|string|min:4|max:64',
            'name'      => 'required|string|min:4|max:64',
            'email'     => 'required|email',
            'post'      => 'required|string|min:4|max:255',
            'biography' => 'required|min:100',
            'schedule'  => 'required|string|min:4|max:64',
            'avatar'    => 'nullable|image',
        ];
    }

    /**
     * Custom messages
     * 
     * @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => \trans('acp/teacher.create.form.fields.email.errors.required'),
            'email.email'    => \trans('acp/teacher.create.form.fields.email.errors.email'),

            'last_name.required' => \trans('acp/teacher.create.form.fields.last_name.errors.required'),
            'last_name.string'   => \trans('acp/teacher.create.form.fields.last_name.errors.string'),
            'last_name.min'      => \trans('acp/teacher.create.form.fields.last_name.errors.min'),
            'last_name.max'      => \trans('acp/teacher.create.form.fields.last_name.errors.max'),

            'name.required' => \trans('acp/teacher.create.form.fields.name.errors.required'),
            'name.string'   => \trans('acp/teacher.create.form.fields.name.errors.string'),
            'name.min'      => \trans('acp/teacher.create.form.fields.name.errors.min'),
            'name.max'      => \trans('acp/teacher.create.form.fields.name.errors.max'),

            'post.required' => \trans('acp/teacher.create.form.fields.post.errors.required'),
            'post.string'   => \trans('acp/teacher.create.form.fields.post.errors.string'),
            'post.min'      => \trans('acp/teacher.create.form.fields.post.errors.min'),
            'post.max'      => \trans('acp/teacher.create.form.fields.post.errors.max'),

            'schedule.required' => \trans('acp/teacher.create.form.fields.schedule.errors.required'),
            'schedule.string'   => \trans('acp/teacher.create.form.fields.schedule.errors.string'),
            'schedule.min'      => \trans('acp/teacher.create.form.fields.schedule.errors.min'),
            'schedule.max'      => \trans('acp/teacher.create.form.fields.schedule.errors.max'),

            'biography.required' => \trans('acp/teacher.create.form.fields.biography.errors.required'),
            'biography.min'      => \trans('acp/teacher.create.form.fields.biography.errors.min'),

            'avatar.image' => \trans('acp/teacher.create.form.fields.avatar.errors.image'),
        ];
    }
}
