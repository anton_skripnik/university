<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\{
    HasMediaTrait,
    Interfaces\HasMedia,
    Interfaces\HasMediaConversions
};

/**
 * Class Gallery
 *
 * @package App\Models
 */
class Gallery extends Model implements HasMedia, HasMediaConversions
{
    use HasMediaTrait;

    /**
     * @var string
     */
    protected $table = 'galleries';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'title' => 'string',
    ];

    /**
     * @return string
     */
    public function getPhotoUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('gallery', 'md');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('sm')
            ->fit(Manipulations::FIT_CROP, 150, 150)
            ->sharpen(10)
            ->performOnCollections('gallery');
        $this->addMediaConversion('md')
            ->fit(Manipulations::FIT_CROP, 250, 250)
            ->sharpen(10)
            ->performOnCollections('gallery');
    }
}
