<?php

declare(strict_types = 1);

namespace App\Models;

use App\Scopes\OrderPositionScope;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

/**
 * Class Graduate
 * 
 * @package App\Models
 */
class Graduate extends Model implements HasMedia, HasMediaConversions
{
    use HasMediaTrait;
    
    /**
     * @var string
     */
    protected $table = 'graduates';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'name',
        'subheader',
        'text',
        'position',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'name'      => 'string',
        'subheader' => 'string',
        'text'      => 'text',
        'email'     => 'string',
        'position'  => 'integer',
    ];

    /**
     * @return string
     */
    public function getPhotoUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('graduates', 'thumb');
    }

    /**
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 200, 245)
            ->sharpen(10)
            ->performOnCollections('graduates');
    }

    /**
     * @return void
     * 
     * @throws \InvalidArgumentException
     */
    protected static function boot()
    {
        static::addGlobalScope(new OrderPositionScope);
    }

}
