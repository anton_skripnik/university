<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\{
    HasMediaTrait,
    Interfaces\HasMedia,
    Interfaces\HasMediaConversions
};
/**
 * Class History
 * 
 * @package App\Models
 */
class History extends Model implements HasMedia, HasMediaConversions
{
    use HasMediaTrait;

    /**
     * @var string
     */
    protected $table = 'histories';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'title'       => 'string',
        'description' => 'text',
    ];

    /**
     * @return string
     */
    public function getPhotoUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('aboutus');
    }

    /**
     * @param Media|null $media
     * 
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('lg')
            ->fit(Manipulations::FIT_CROP, 800, 500)
            ->sharpen(10)
            ->performOnCollections('aboutus');

        $this->addMediaConversion('md')
            ->fit(Manipulations::FIT_CROP, 400, 250)
            ->sharpen(10)
            ->performOnCollections('aboutus');

        $this->addMediaConversion('sm')
            ->fit(Manipulations::FIT_CROP, 200, 125)
            ->sharpen(10)
            ->performOnCollections('aboutus');
    }
}
