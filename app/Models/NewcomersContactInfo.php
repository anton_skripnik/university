<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewcomersContactInfo extends Model
{
    protected $table = 'newcomers_contact_info';

    protected $fillable = [ 'email', 'cell', 'school' ];
}
