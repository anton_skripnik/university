<?php

declare(strict_types = 1);

namespace App\Models;

use App\Scopes\OrderDateScope;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\{
    HasMediaTrait,
    Interfaces\HasMedia,
    Interfaces\HasMediaConversions
};

/**
 * Class News
 * 
 * @package App\Models
 */
class News extends Model implements HasMedia, HasMediaConversions
{
    use HasMediaTrait;
    
    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'author',
        'slug',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'title'       => 'string',
        'description' => 'text',
        'author'      => 'string',
        'slug'        => 'string',
    ];

    /**
     * @return string
     */
    public function getPhotoUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('news');
    }

    /**
     * @param Media|null $media
     * 
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('lg')
            ->fit(Manipulations::FIT_CROP, 800, 500)
            ->sharpen(10)
            ->performOnCollections('news');

        $this->addMediaConversion('md')
            ->fit(Manipulations::FIT_CROP, 400, 250)
            ->sharpen(10)
            ->performOnCollections('news');

        $this->addMediaConversion('sm')
            ->fit(Manipulations::FIT_CROP, 200, 125)
            ->sharpen(10)
            ->performOnCollections('news');
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return void
     * 
     * @throws \InvalidArgumentException
     */
    protected static function boot()
    {
        static::addGlobalScope(new OrderDateScope);
    }
}
