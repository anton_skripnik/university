<?php

declare(strict_types = 1);

namespace App\Models\Teacher;

use App\Scopes\OrderPositionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Article
 * 
 * @package App\Models\Teacher
 */
class Article extends Model
{
    /**
     * @var string
     */
    protected $table = 'articles';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'link',
        'description',
        'year',
        'teacher_id',
        'position',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'link'        => 'string',
        'description' => 'text',
        'year'        => 'integer',
        'teacher_id'  => 'integer',
        'position'    => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * @return void
     * 
     * @throws \InvalidArgumentException
     */
    protected static function boot()
    {
        static::addGlobalScope(new OrderPositionScope);
    }
}
