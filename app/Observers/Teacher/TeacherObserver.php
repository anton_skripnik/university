<?php

declare(strict_types = 1);

namespace App\Observers\Teacher;

use App\Models\Teacher\Teacher;

/**
 * Class TeacherObserver
 * 
 * @package App\Observers\Teacher
 */
class TeacherObserver
{
    /**
     * @param Teacher $teacher
     */
    public function saving(Teacher $teacher)
    {
        $teacher->forceFill(['slug' => $teacher->createSlug()]);
    }
}