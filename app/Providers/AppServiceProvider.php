<?php

namespace App\Providers;

use App\Models\Contact;
use App\Models\Teacher\Teacher;
use App\Observers\Teacher\TeacherObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Teacher::observe(TeacherObserver::class);

        //contacts on sidebar
        view()->composer('client.layouts.partial.sidebar',
            function($view) {
                $view->with('contacts', Contact::first());
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
