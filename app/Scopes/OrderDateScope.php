<?php

declare(strict_types = 1);

namespace App\Scopes;

use Illuminate\Database\Eloquent\{
    Scope, Model, Builder
};

/**
 * Class OrderDateScope
 * 
 * @package App\Scopes
 */
class OrderDateScope implements Scope
{
    /**
     * Default order is LIFO
     */
    const DEFAULT_ORDER_BY = 'created_at';

    /**
     * @var string
     */
    protected $orderBy;

    /**
     * OrderDateScope constructor.
     *
     * @param string $orderBy
     */
    public function __construct(string $orderBy = self::DEFAULT_ORDER_BY)
    {
        $this->orderBy = $orderBy;
    }

    /**
     * Global LIFO order scope
     * 
     * @param  Builder $builder
     * @param  Model   $model
     * 
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->orderByDesc($this->orderBy);
    }
}