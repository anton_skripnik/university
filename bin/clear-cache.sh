#!/usr/bin/env bash

echo "Clearing cache ..."
php artisan cache:clear
php artisan view:clear
php artisan clear-compiled
echo "-> Success"
echo "Clearing Memcached on 127.0.0.1:11211..."
echo "flush_all" | nc -q 2 localhost 11211;
echo "-> Success"
