#!/usr/bin/env bash

branch=$(git rev-parse --abbrev-ref HEAD)
remote=$(git remote)
git pull "$remote" "$branch"

changedFiles="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"
checkRun() {
    echo "$changedFiles" | grep --quiet "$1" && eval "$2"
}

composer dumpautoload

checkRun "composer.lock" "composer install --no-suggest"
checkRun "yarn.lock" "yarn install"
checkRun "database/migrations" "rm -rf public/media/* && php artisan migrate:fresh --seed && php artisan db:seed --class=TestDatabaseSeeder"
./bin/clear-cache.sh
yarn dev
php artisan horizon:terminate
