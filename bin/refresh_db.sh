#!/usr/bin/env bash

rm -rf public/media/* && php artisan migrate:fresh --seed && php artisan db:seed --class=FakeDataSeeder
