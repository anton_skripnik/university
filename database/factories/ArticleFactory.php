<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Teacher\Article::class, function (Faker $faker) {
    return [
        'description' => $faker->text,
        'link' => $faker->url,
    ];
});
