<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Contact::class, function (Faker $faker) {
    return [
        'phone'   => $faker->phoneNumber,
        'address' => $faker->address,
    ];
});
