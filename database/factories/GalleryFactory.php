<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Gallery::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
    ];
});
