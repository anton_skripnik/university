<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Graduate::class, function (Faker $faker) {
    return [
        'name'      => $faker->lastName.' '.$faker->firstName,
        'subheader' => $faker->sentence,
        'text'      => $faker->text,
    ];
});
