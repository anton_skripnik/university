<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Link\LinkCategory::class, function (Faker $faker) {
    return [
        'category' => $faker->word,
        'icon'     => 'list-ul',
    ];
});
