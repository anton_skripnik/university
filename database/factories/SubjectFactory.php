<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Teacher\Subject::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(4),
    ];
});
