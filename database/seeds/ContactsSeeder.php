<?php

declare(strict_types = 1);

use App\Models\Contact;
use Illuminate\Database\Seeder;

/**
 * Class ContactsSeeder
 */
class ContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(\App\Models\Contact::class, 1)->create();
        Contact::create([
            'address' => 'м. Запоріжжя, вул. Жуковського, 66',
        ]);
    }
}
