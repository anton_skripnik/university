<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class GallerySeeder
 */
class GallerySeeder extends Seeder
{
    use HasMediaTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photos = factory(\App\Models\Gallery::class, 27)->create();

        foreach ($photos as $photo) {
            $faker = \Faker\Factory::create();
            $photo->addMediaFromUrl($faker->imageUrl())->toMediaCollection('gallery');
        }

    }
}
