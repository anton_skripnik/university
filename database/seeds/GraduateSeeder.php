<?php

declare(strict_types = 1);

use App\Models\Graduate;
use Illuminate\Database\Seeder;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Faker\Factory;

/**
 * Class GraduateSeeder
 */
class GraduateSeeder extends Seeder
{
    use HasMediaTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $graduates = factory(Graduate::class, 7)->create();

        foreach ($graduates as $graduate) {
            $faker = Factory::create();
            $graduate->addMediaFromUrl($faker->imageUrl())->toMediaCollection('graduates');
        }
    }
}
