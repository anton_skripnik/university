<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Faker\Factory;

/**
 * Class HistorySeeder
 */
class HistorySeeder extends Seeder
{
    use HasMediaTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $history = factory(\App\Models\History::class, 1)->create();

        foreach ($history as $item) {
            $faker = Factory::create();
            for ($i = 0; $i < 7; $i++) {
                $item->addMediaFromUrl($faker->imageUrl())->toMediaCollection('aboutus');
            }
        }
    }
}
