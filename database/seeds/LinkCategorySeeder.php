<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use App\Models\Link\Link;
use App\Models\Link\LinkCategory;

/**
 * Class LinkCategorySeeder
 */
class LinkCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LinkCategory::class, 6)->create()
            ->each(function ($c) {
                $c->links()->saveMany(factory(Link::class, 7)->make());
            });
    }
}
