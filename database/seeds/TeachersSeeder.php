<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use App\Models\Teacher\{
    Teacher, Subject, Article
};
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Faker\Factory;

/**
 * Class TeachersSeeder
 */
class TeachersSeeder extends Seeder
{
    use HasMediaTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teachers = factory(Teacher::class, 7)->create()
            ->each(function ($t) {
                $t->subjects()->saveMany(factory(Subject::class, 7)->make());
                $t->articles()->saveMany(factory(Article::class, 25)->make());
            });

        foreach ($teachers as $teacher) {
            $faker = Factory::create();
            $teacher->addMediaFromUrl($faker->imageUrl())->toMediaCollection('teachers');
        }
    }
}
