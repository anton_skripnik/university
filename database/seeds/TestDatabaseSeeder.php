<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;

/**
 * Class TestDatabaseSeeder
 */
class TestDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(NewsSeeder::class);
        $this->call(TeachersSeeder::class);
        $this->call(GallerySeeder::class);
        $this->call(HistorySeeder::class);
        $this->call(LinkCategorySeeder::class);
        $this->call(ContactsSeeder::class);
        $this->call(GraduateSeeder::class);
    }
}
