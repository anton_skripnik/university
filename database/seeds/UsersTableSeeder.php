<?php

declare(strict_types = 1);

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Call deps
        $this->call(RolesSeeder::class);

        /** @var User $tech */
        $admin = User::create([
            'name'     => 'Admin',
            'email'    => 'admin@vsr.com',
            'password' => bcrypt('password'),
        ]);

        $admin->assignRole('admin');

        $tech = User::create([
            'name'     => 'Tech',
            'email'    => 'tech@vsr.com',
            'password' => bcrypt('password'),
        ]);

        $tech->assignRole('root');
    }
}
