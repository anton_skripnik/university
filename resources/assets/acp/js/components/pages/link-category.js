import {http} from "../../http";
import {vbus} from "../../vbus";

Vue.component('link-category', {

    props: ['data'],

    data() {
        return {
            rows: [{
                id: '',
                category: {n: 'category[0]', v: ''},
                icon: {n: 'icon[0]', v: ''},
            }]

        }
    },

    mounted() {
        this.rows = JSON.parse(this.data);
    },

    methods: {

        edit(id) {
            $('.link-category-block_'+id).hide();
            $('#edit-link-category-block_'+id).css('display', 'table');
        },

        update(category, icon, id) {
            http.patch(window.laroute.route('acp.link.category.update', {category: id}), {
                category: category,
                icon: icon,
            })
                .then(response => {
                    $('.link-category-block_'+id).show();
                    $('#edit-link-category-block_'+id).hide();
                })
                .catch(e => {
                    console.log(e)
                })
        },

        destroy(id) {
            let self = this
            http.delete(window.laroute.route('acp.link.category.destroy', {category: id}))
                .then(response => {
                    if (response.data.status === 500) {
                        vbus.$emit('showAlert',
                            { severity: 'warning', message: response.data.data })
                    } else {
                        this.rows.forEach(function(value, index) {
                            if (value.id === id) {
                                self.rows.splice(index, 1)
                            }
                        });
                    }
                })
                .catch(e => {
                    console.log(e)
                })
        }
    }
});