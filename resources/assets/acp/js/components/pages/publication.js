import {http} from "../../http";
import Sortable from 'sortablejs'
import Pagination from 'vue2-laravel-pagination'

Vue.component('publication', {

    props: ['data', 'pages', 'teacher'],

    components: {
        'pagination': Pagination
    } ,

    data() {
        return {
            rows: [{
                id: '',
                article: {n: 'article[0][description]', v: '',},
                link: {n: 'article[0][link]', v: '',},
            }],
            page: {current_page: 1, total: 1, per_page: 1},
        }
    },

    created() {
        http.get(window.laroute.route('acp.teacher.article.show', {teacher: this.teacher}))
            .then(response => {
                this.rows = response.data[0];
                this.page = response.data[1];
            })
            .catch(err => console.log(err));
    },

    mounted() {
        let sortable = Sortable.create($('#article-sort')[0], {
            handle: '.handle',
            onEnd: () => {

                let data = {};

                $( "#article-sort li" ).each(function() {
                    data[+$( this ).index()] = +$(this).attr('id');
                });

                http.patch(window.laroute.route('acp.teacher.article.update.position', {teacher: this.teacher}), {
                    data: data
                })
                    .then(response => {
                        console.log(response)
                    })
                    .catch(e => {
                        console.log(e)
                    })
            },
        });
    },

    methods: {

        fetchData(page) {
            page = page || 1;
            http.get(window.laroute.route('acp.teacher.article.show', {paginate: 'paginate', teacher: this.teacher}) + '?page=' + page)
                .then(response => {
                    this.rows = response.data[0]
                    this.page.current_page = page
                })
                .catch(err => console.log(err))
        },

        getAll() {
            http.get(window.laroute.route('acp.teacher.article.show', {paginate: 'all', teacher: this.teacher}))
                .then(response => {
                    this.rows = response.data[0];
                    this.page.per_page = 1;
                    this.page.total = 0;
                })
                .catch(err => console.log(err))
        },

        add() {
            const n = this.rows.length;

            this.rows.push({
                article: {n: 'article['+ n +'][description]', v: '',},
                link: {n: 'article['+ n +'][link]', v: '',},
            });
        },

        del(row) {
            const index = this.rows.indexOf(row);
            this.rows.splice(index, 1);
        },

        edit(id) {
            $('#article-block_'+id).hide();
            $('#edit-article-block_'+id).css('display', 'flex');
        },

        update(name, link, id) {
            http.patch(window.laroute.route('acp.teacher.article.update', {article: id}), {
                name: name,
                link: link,
            })
                .then(response => {
                    $('#article-block_'+id).show();
                    $('#edit-article-block_'+id).hide();
                })
                .catch(e => {
                    console.log(e)
                })
        },

        destroy(id) {
            let self = this;
            http.delete(window.laroute.route('acp.teacher.article.destroy', {article: id}))
                .then(() => {
                    this.rows.forEach(function(value, index) {
                        if (value.id === id) {
                            self.rows.splice(index, 1)
                        }
                    });
                })
                .catch(e => {
                    console.log(e)
                })
        }
    }
});