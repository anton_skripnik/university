import {http} from "../../http";
import Sortable from "sortablejs";

Vue.component('subject', {

    props: ['data'],

    data() {
        return {
            rows: [{
                id: '',
                name: {n: 'subject[0]', v: ''},
            }]

        }
    },

    mounted() {
        this.rows = JSON.parse(this.data);

        let sortable = Sortable.create($('#subject-sort')[0], {
            handle: '.handle',
            onEnd: () => {

                let data = {};

                $( "#subject-sort li" ).each(function() {
                    data[+$( this ).index()] = +$(this).attr('id');
                });

                http.patch(window.laroute.route('acp.teacher.subject.update.position'), {
                    data: data
                })
                    .then(response => {
                        console.log(response)
                    })
                    .catch(e => {
                        console.log(e)
                    })

            },
        });
    },

    methods: {
        add() {
            const n = this.rows.length;

            this.rows.push({
                name: {n: 'subject[' + n + ']', v: ''},
            });
        },

        del(row) {
            const index = this.rows.indexOf(row);
            this.rows.splice(index, 1);
        },

        edit(id) {
          $('#subject-block_'+id).hide();
          $('#edit-subject-block_'+id).css('display', 'table');
        },

        update(name, id) {
            http.patch(window.laroute.route('acp.teacher.subject.update', {subject: id}), {
                name: name,
            })
                .then(response => {
                    $('#subject-block_'+id).show();
                    $('#edit-subject-block_'+id).hide();
                })
                .catch(e => {
                    console.log(e)
                })
        },

        destroy(id) {
            let self = this
            http.delete(window.laroute.route('acp.teacher.subject.destroy', {subject: id}))
            .then(() => {
                this.rows.forEach(function(value, index) {
                    if (value.id === id) {
                        self.rows.splice(index, 1)
                    }
                });
            })
                .catch(e => {
                    console.log(e)
                })
        }
    }
});