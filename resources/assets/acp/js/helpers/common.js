import {http} from "../http";
import Sortable from "sortablejs";
import SimpleMDE from "simplemde";

$('[data-toggle="tooltip"]').tooltip({
  container: 'body'
});

$('.markdown_editor').each(function() {
    let simplemde = new SimpleMDE({
        element: this,
    });
    simplemde.render();
});

//for creating and/or choosing link`s categories
if($('select#category-select').length > 0) {
    $('select#category-select').select2({
        tags: true
    });
}

//change teachers position
if($('tbody#teacher-sort').length > 0) {

    let sortable = Sortable.create($('#teacher-sort')[0], {
        handle: '.handle',
        onEnd: () => {

            let data = {};

            $("#teacher-sort tr").each(function () {
                data[+$(this).index()] = +$(this).data('id');
            });

            http.patch(window.laroute.route('acp.teacher.update.position'), {
                data: data
            })
                .then(response => {
                    console.log(response)
                })
                .catch(e => {
                    console.log(e)
                })

        },
    });
};

//change graduates position
if($('tbody#graduate-sort').length > 0) {

    let sortable = Sortable.create($('#graduate-sort')[0], {
        handle: '.handle',
        onEnd: () => {

            let data = {};

            $("#graduate-sort tr").each(function () {
                data[+$(this).index()] = +$(this).data('id');
            });

            http.patch(window.laroute.route('acp.graduate.update.position'), {
                data: data
            })
                .then(response => {
                    console.log(response)
                })
                .catch(e => {
                    console.log(e)
                })
        },
    });
};

function bsTabToHash() {
    let hash = document.location.hash;
    let prefix = "tab_";
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    }

  // Change hash for page-reload
    $('.nav-tabs a, .menu-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash.replace("#", "#" + prefix);
    });
}
bsTabToHash();


