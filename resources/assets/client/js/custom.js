import Swiper from 'swiper/dist/js/swiper.min'
import {controller} from 'swiper'

if ($('.gallery-plugin').length) {
    let galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    let galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true,
    });
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;
}

if ($('.swiper-container').length > 0) {
    if ($( window ).width() > 1280) {
        $('#sidebar').css('margin-right', '-24em');
        $('#main').css('padding-left', '21.5em');
    }
}

function bsTabToHash() {
    let hash = document.location.hash;
    let prefix = "tab_";
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a, .menu-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash.replace("#", "#" + prefix);
    });
}
bsTabToHash();

