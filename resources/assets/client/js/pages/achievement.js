import {http} from "../http";

Vue.component('achievement', {

    data() {
        return {
            achievements: [{
                id: '',
                title: '',
                description: '',
                image: '',
                slug: '',
            }],
            page: {current_page: 1, total: 1, per_page: 1},
        }
    },

    created() {
        http.get(window.laroute.route('achievement.showMore'))
            .then(response => {
                console.log(response);
                this.achievements = response.data[0];
                this.page = response.data[1];
            })
            .catch(err => console.log(err))
    },

    methods: {
        fetchData(page) {

            page = page || 1;
            http.get(window.laroute.route('achievement.showMore') + '?page=' + page)
                .then(response => {
                    for(let i = 0; i < response.data[0].length; i++) {
                        this.achievements.push(response.data[0][i])
                    }
                    this.page.current_page = page
                })
                .catch(err => console.log(err))
        },
        goToPage(slug) {
            window.location.href = window.laroute.route('achievement.show', {achievement: slug});
        },
    },
});