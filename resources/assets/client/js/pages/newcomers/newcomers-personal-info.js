import modal from 'vue-js-modal'
import axios from 'axios'

Vue.use(modal, { 
  dynamic: true,
  dialog: true,
  dynamicDefaults: {
    clickToClose: true,
  },
  resizable: true,
  adaptive: true,
  injectModalsContainer: true
})

Vue.component("newcomers-personal-info", {
  computed: {
    emailValidationErrorMessage: function() {
      if (this.validationErrors === undefined || this.validationErrors.email === undefined || this.validationErrors.email[0] === undefined) {
        return undefined
      }

      return this.validationErrors.email[0]
    },
    cellValidationErrorMessage: function() {
      if (this.validationErrors === undefined || this.validationErrors.cell === undefined || this.validationErrors.cell[0] === undefined) {
        return undefined
      }

      return this.validationErrors.cell[0]
    },
    schoolValidationErrorMessage: function() {
      if (this.validationErrors === undefined || this.validationErrors.school === undefined || this.validationErrors.school[0] === undefined) {
        return undefined
      }

      return this.validationErrors.school[0]
    },
  },
  data: function() {
    const panes = {
      consent: 'newcomers-personal-info-consent-pane',
      input: 'newcomers-personal-info-input-pane',
      goodies: 'newcomers-personal-info-goodies-pane',
    }
    return {
      panes: panes,
      currentPane: panes.consent,
      contactData: { 
        email: '',
        cell: '',
        school: '',
      },
      validationErrors: undefined,
      requestInProgressModalName: 'request-in-progress-modal',
      genericErrorModalName: 'generic-error-modal',
    }
  },
  methods: {
    proceedToProvidingPersonalInfo: function() {
      this.currentPane = this.panes.input
    },
    proceedToGoodiesScreen: function() {
      this.currentPane = this.panes.goodies
    },
    proceedWithoutProvidingPersonalInfo: function() {
      this.$emit('openTasks')
    },
    proceedToTasksList: function() {
      this.$emit('openTasks')
    },
    submitPersonalInfoAndProceed: function() {
      let that = this

      that.displayRequestInProgressUI()


      axios.post('/newcomers-contact-info', {
        email: that.contactData.email,
        cell: that.contactData.cell,
        school: that.contactData.school,
      }).then(function(response) {
        that.hideRequestInProgressUI()

        that.proceedToGoodiesScreen()
      }).catch(function(error) {
        that.hideRequestInProgressUI()

        if (error.response.status === 422 && error.response.data.errors !== undefined) {
          that.displayValidationErrors(error.response.data.errors)
        } else {
          that.displayGenericErrorAlert()
        }
      })
    },

    displayRequestInProgressUI: function() {
      this.$modal.show({
        template: `
          <div>
            <img src="/build/client/images/spinner.svg"/>
            <p style='text-align:center'>Чекайте...</p>
          </div>
        `,
      }, {}, {
        name: this.requestInProgressModalName,
        width: '150px',
        height: 'auto',
        clickToClose: false,
      })
    },

    hideRequestInProgressUI: function() {
      this.$modal.hide(this.requestInProgressModalName)
    },

    displayValidationErrors: function(errors) {
      this.validationErrors = errors
    },

    displayGenericErrorAlert: function() {
      const that = this
      this.$modal.show(this.genericErrorModalName, {
        title: 'Помилка',
        text: 'Сталась помилка при відправці даних. Спробуйте, будь ласка, ще раз пізніше.',
        buttons: [
          {
            title: 'ОК',
            handler: function() {
              that.$modal.hide(that.genericErrorModalName)
            }
          }
        ]
      })
    },
  },
  template: `
    <div class='newcomers-personal-info-consent-container'>
      <v-dialog />

      <div class='newcomers-personal-info-consent-pane' v-if='currentPane === panes.consent'>
        <div><h2>Передати дані</h2></div>
        <div><p>Наша кафедра для власних статистичних цілей збирає дані про потенційних абітурієнтів. Якщо ви бажаєте, можете передати нам власні дані в обмін на деякі ніштяки :)</p></div>

        <div><a class='button big' href=# v-on:click.prevent='proceedToProvidingPersonalInfo'><span>Передати інформацію</span></a></div>
        <div><a class='button big' href=# v-on:click.prevent='proceedWithoutProvidingPersonalInfo'><span>Продовжити інкогніто</span></a></div>
      </div>

      <div class='newcomers-personal-info-input-pane' v-if='currentPane === panes.input'>
        <div class='newcomers-personal-info-input-pane-section'>
          <h3>Познайомимось? 🙃</h3>
          <div class='newcomers-personal-info-input-pane-fineprint'>Ми не передаватимемо Ваші дані третім сторонам, а використовуватимемо лише всередині кафедри для статистики</div>
        </div>
        <div class='newcomers-personal-info-input-pane-section'>
          <div class='newcomers-personal-info-input-pane-input-label'>
            <span>E-mail адреса</span>
            <span class='newcomers-personal-info-input-pane-validation-error-label' v-if='emailValidationErrorMessage !== undefined'>{{ emailValidationErrorMessage }}</span>
          </div> 
          <input id='email' type=email v-model='contactData.email' v-bind:class='{ "failed-validation": emailValidationErrorMessage !== undefined }'></input>
        </div>
        <div class='newcomers-personal-info-input-pane-section'>
          <div class='newcomers-personal-info-input-pane-input-label'>
            <span>Мобільний телефон</span>
            <span class='newcomers-personal-info-input-pane-validation-error-label' v-if='cellValidationErrorMessage !== undefined'>{{ cellValidationErrorMessage }}</span>
          </div> 
          <input id='phone' type=tel v-model='contactData.cell' v-bind:class='{ "failed-validation": cellValidationErrorMessage !== undefined }'></input>
        </div>
        <div class='newcomers-personal-info-input-pane-section'>
          <div class='newcomers-personal-info-input-pane-input-label'>
            <span>Навчальний заклад</span>
            <span class='newcomers-personal-info-input-pane-validation-error-label' v-if='schoolValidationErrorMessage !== undefined'>{{ schoolValidationErrorMessage }}</span>
          </div> 
          <input id='school' type=text v-model='contactData.school' v-bind:class='{ "failed-validation": schoolValidationErrorMessage !== undefined }'></input>
        </div>
        <div class='newcomers-personal-info-input-pane-section'>
          <a class='button big' href=# v-on:click.prevent='submitPersonalInfoAndProceed'><span>Передати та отримати ніштяк!</span></a>
        </div>
        <div class='newcomers-personal-info-input-pane-section'>
          <a class='button big' href=# v-on:click.prevent='proceedWithoutProvidingPersonalInfo'><span>Продовжити інкогніто</span></a>
        </div>
      </div>

      <div class='newcomers-personal-info-goodies-pane' v-if='currentPane === panes.goodies'>
        <div><h3>Приємно познайомитись!</h3></div>
        <div>
          <p>Дякуємо Вам! Як символ нашої вдячності, пропонуємо Вам:</p>
          <ul>
            <li>Скачати набір стікерів для <a href='https://telegram.org/'>Телеграм</a>: <a href='https://t.me/addstickers/vsr_znu'>тиць</a></li>
            <li>Пограти в кльову гру 🤙: <a href='https://test-clean-the-library.herokuapp.com/'>туць</a></li>
          </ul>
          <p>А після цього мерщій переходьте до завдань, які ми підготували!</p>
        </div>

        <div><a class='button big' href=# v-on:click.prevent='proceedToTasksList'><span>До завдань</span></a></div>
      </div>
    </div>
  `
})
