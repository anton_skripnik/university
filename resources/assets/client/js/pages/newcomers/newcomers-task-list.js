import truncate from 'vue-truncate-collapsed'
import newspaperTask from './tasks/newcomers-task-newspaper.vue'
import fakesTask from './tasks/newcomers-task-fakes.vue'
import grammarTask from './tasks/newcomers-task-grammar.vue'

require('./tasks/newcomers-task-logo')
require('./tasks/newcomers-task-vocabulary')
require('./tasks/newcomers-task-punctuation')
require('./tasks/newcomers-task-rhethoric-hints')
require('./tasks/newcomers-task-feminitives')

Vue.component("newcomers-task-list", {
  components: {
    'truncate': truncate,
  },
  data: function() {
   return {
     executedTask: null,
     tasks: [
       {
         component: "newcomers-task-logo",
         image: "/build/client/images/task-list/task-list-icon-books.png",
         title: "Фестиваль загадковості у видавничому середовищі",
         text: "У моряків є таке повір'я: як корабель назвеш, так він і попливе. У видавничому бізнесі назва та логотип також вважаються суттєвими деталями, без яких важко рухатися. Пропонуємо Вам пройти перевірку та з'ясувати свій рівень знань у вітчизняній видавничій справі. Оскільки українські видавці добре взаємодіють з фантазією, то кожному, хто пройде перевірку, випаде можливість навчитися чомусь новому. Розширюємо світогляд разом!"
       },
       {
         component: "newcomers-task-vocabulary",
         image: "/build/client/images/task-list/task-list-icon-typewriter.png",
         title: "Не суди по назві &mdash; суди по змісту",
         text: "У видавничій справі не завжди вживаються «сухі» назви, важкі терміни та багато чого незрозумілого. Деякі сучасні поліграфічні приклади ми зараз розглянемо, але в особливій формі - формі загадок."
       },
       {
         component: "newcomers-task-feminitives",
         image: "/build/client/images/task-list/task-list-icon-soundcone.png",
         title: "Рівноправність = професіоналізм",
         text: "<p>В епоху гендерної рівності необхідно завжди використовувати фемінітиви. Тим паче, що українська мова корелює з ними. Ця тема є особливо важливою, адже, наприклад, коли на співбесіді кажеш «видавчиха»  чи «редагиня», то існує ймовірність, що доведеться йти працювати до супермаркету. У кращому випадку. Або на  Facebook у палкій дискусії ви втратите перевагу, якщо поставитесь до фемінітивів зверхньо.</p><p>Проаналізуємо деякі слова з видавничої справи, щоби у майбутньому не потрапити в скрутне становище.</p>"
       },
       {
         component: "newcomers-task-punctuation",
         image: "/build/client/images/task-list/task-list-icon-hand.png",
         title: "Грамотність &mdash; вагома валюта",
         text: "Якщо Вам ріже око, коли Ви бачите помилки в текстах, то це завдання точно стане в пригоді. І щоб пригода запам'яталась надовго, до тексту потрібно підходити з любов'ю та дуже прискіпливо. Оскільки зараз в моді грамотність, а тренди народ підхоплює, тому пропонуємо Вам йти в ногу з часом та почати свій шлях з редагування тексту!"
       },
       {
         component: fakesTask,
         componentName: "newcomers-task-fakes",
         image: "/build/client/images/task-list/task-list-icon-questions.png",
         title: "Крізь брехню і маніпуляції до об'єктивності",
         text: "<p>В епоху інформаційного розвитку багато ризиків натрапити на фейк. За їх допомогою населенням легко маніпулювати, адже за використання неправдивих відомостей у певної людини може сформуватися хибне ставлення до якоїсь ситуації чи відомої персони.</p><p>Редактору видання важливо не допустити потрапляння фальсифікації на шпальту. Потрібно вміти мислити критично та бути уважним до деталей.</p><p>Тому пропонуємо Вам пройти через декілька новин, щоби вирішити, фейк це чи ні.</p>"
       },
       {
         component: "newcomers-task-rhethoric-hints",
         image: "/build/client/images/task-list/task-list-icon-mic.png",
         title: "Риторика &gt; страх публіки та вербальна бідність",
         text: "Майстри красномовства були завжди потрібні. Володіючи певними тонкощами ораторського мистецтва, можна і книгу просунути вдало, і сніг в люту зиму вигідно продати. Радимо Вам опанувати деякі наші вправи, щоб у майбутньому з позиції сили дискутувати з друзями, адже коректне згадування в реченні такого явища, як софізм, зробить Вашу промову більш суттєвою."
       },
       {
         component: newspaperTask,
         componentName: "newcomers-task-newspaper",
         image: "/build/client/images/task-list/task-list-icon-newspaper.png",
         title: "Невідомий простір: по той бік газети",
         text: "Безліч пасток підстерігає редактора: відповідність заголовка та тексту, правдивість фотоматеріалів, правильність формулювання заголовка. Все це необхідно уважно перевіряти, перш ніж відправити газету до друку. Спробуйте себе у ролі головного редактора – важливо сформувати газету без помилок. За кожну правильну відповідь нараховується віртуальні гроші – прибуток, який отримує газета з продажів.",
       },
       {
         component: grammarTask,
         componentName: "newcomers-task-grammar",
         image: "/build/client/images/task-list/task-list-icon-trashcan.png",
         title: "На сторожі чистоти мови",
         text: "Шкільні вчителі стверджували, що титульна сторінка щоденника – обличчя його власника. Так і в мові: перш за все в цивілізованому суспільстві дивляться на те, як ти спілкуєшся з людьми та яка чистота твого мовлення. Від цього залежить усвідомлення твого рівня освіти та подальші відносини з освіченими людьми. Тому потрібно постійно збагачувати свій словниковий запас, а найголовніше – стежити за його чистотою. Пропонуємо Вам перевірити себе, відшукавши правильні відповідники!"
       },
     ]
   }
  },
  methods: {
    handleBegin: function(task) {
      const that = this

      const componentNameOf = function(task) {
        return (typeof task.component === "string") ? task.component : task.componentName
      }

      const closeExistingModal = function() {
        if (that.executedTask) {
          const modalName = 
          that.$modal.hide(componentNameOf(that.executedTask))
          that.executedTask = null
        }
      }

      if (this.executedTask) {
        closeExistingModal()
      }

      const vueComponent = (typeof task.component === "string") ? Vue.component(task.component) : task.component

      if (!vueComponent) {
        console.error(`No component registered with name ${task.component}`)
        return
      }

      this.$modal.show(vueComponent, {
          'completeTaskTarget': this,
      }, {
          name: componentNameOf(task),
          width: '100%',
          height: '100%',
          clickToClose: false,
      })
      this.$on('completeTask', function(e) {
        closeExistingModal()

        // Enable scrolling of the document
        document.documentElement.classList.remove('newcomers-task-disable-scroll')
      })

      // Disable scrolling of the document
      document.documentElement.classList.add('newcomers-task-disable-scroll')

      this.executedTask = task
    }

  },
  template: `
    <div class='newcomers-task-list-container'>
      <div class='newcomers-task-list'>
        <article v-for='task in tasks'>
          <div class='task-main'>
            <div class='task-head'>
              <img v-bind:src='task.image' v-if='task.image !== undefined'/>
              <h3 v-if='task.title !== undefined' v-html='task.title'></h3>
            </div>
            <div class='description'>
              <truncate clamp='Ще' less='Менше' :length=150 type='html' v-bind:text='task.text'></truncate>
            </div>
          </div>
          <div class='button' v-on:click.prevent='handleBegin(task)'><span>Почати</span></div>
        </article>
      </div>
    </div>
  `,
})
