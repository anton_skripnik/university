Vue.component("newcomers-welcome", {

  data: function() {
    return {
    }
  },

  methods: {
    handleStart: function(action) {
      this.$emit('start')
    },
  },

  template: `
    <div class='newcomers-welcome-container'>
      <div class='header-container'>
        <div class='title-container'>
          <h2>Випробуй</h2>
          <h4>редакторські</h4>
          <h4>здібності</h4>

          <div class='sub-header'>
            Інтерактивні завдання для абітурієнтів
          </div>
        </div>

        <div class='title-icon-container'>
          <img src='/build/client/images/newcomers-welcome/title-icons.png'/>
        </div>
      </div>

      <div class='possibility-list-container'>
        <div class='possibility-header'>
          <h2>Це чудова нагода:</h2>
        </div>

        <div class='possibility-list'>
          <div class='possibility'>
            <div class='icon-container'><img src='/build/client/images/newcomers-welcome/icon-pc.png'/></div>
            <div class='text'><span>відкрити завісу таємниці "що ж вивчають на парах наші студенти"</span></div>
          </div>
          <div class='possibility'>
            <div class='icon-container'><img src='/build/client/images/newcomers-welcome/icon-photo.png'/></div>
            <div class='text'><span>дізнатися багато цікавого та пізнавального</span></div>
          </div>
          <div class='possibility'>
            <div class='icon-container'><img src='/build/client/images/newcomers-welcome/icon-newspaper.png'/></div>
            <div class='text'><span>спробувати себе в ролі видавця, редактора та коректора</span></div>
          </div>
          <div class='possibility'>
            <div class='icon-container'><img src='/build/client/images/newcomers-welcome/icon-book.png'/></div>
            <div class='text'><span>зробити вибір на користь освітньої програми</span></div>
          </div>
        </div>
      </div>

      <div class='button-container'>
        <a class='button big' @click.prevent='handleStart'><span>Розпочати</span></a>
      </div>
    </div> 
  `
})
