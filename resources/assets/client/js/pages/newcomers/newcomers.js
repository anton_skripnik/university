require('./newcomers-welcome')
require('./newcomers-personal-info')
require('./newcomers-task-list')

Vue.component("newcomers", {

  data: function() {
    const subcomponents = {
        welcome: "newcomers-welcome", 
        personalInfo: "newcomers-personal-info",
        taskList: "newcomers-task-list"
    }

    return {
      subcomponents: subcomponents,
      currentComponent: subcomponents.welcome
    }
  },

  mounted: function() {
    this.currentComponent = this.subcomponents.welcome
  },

  methods: {
    handleStartEvent: function() {
      this.$data.currentComponent = this.$data.subcomponents.personalInfo
    },
    handleOpenTasksEvent: function() {
      this.$data.currentComponent = this.$data.subcomponents.taskList
    }
  },

  template: `
    <div>
      <newcomers-welcome v-if='currentComponent === subcomponents.welcome' v-on:start='handleStartEvent'>
      </newcomers-welcome>

      <newcomers-personal-info v-if='currentComponent === subcomponents.personalInfo' v-on:openTasks='handleOpenTasksEvent'>
      </newcomers-personal-info>

      <newcomers-task-list v-if='currentComponent === subcomponents.taskList'>
      </newcomers-task-list>
    </div>
  `
})

