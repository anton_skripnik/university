Vue.component('newcomers-task-feminitives', {
  template:`
    <div class='task-feminitives-container'>
      <div class='close-button' v-on:click='confirmAndCloseTask()'>❌</div>
      <v-dialog />

      <div class='task-container'>
        <div class='task-main'>
          <div class='header-container'>
            <h2>Фемінітиви!</h2>
          </div>
          <div class='page-container'>
            <div class='page-info' v-if='currentPage.type === "info"'>
              <p class='info-paragraph' v-for='paragraph in currentPage.paragraphs'>
                {{ paragraph }}
              </p>
            </div>
            <div class='page-quiz' v-if='currentPage.type === "quiz"'>
              <div class='quiz-text'>
                {{ currentPage.text }}
              </div>

              <ul class='quiz-options'>
                <li class='quiz-option' 
                    v-for='option in currentPage.options' 
                    v-on:click.prevent.stop='selectQuizOption(option)' 
                    v-bind:class='quizOptionClass(option)'>
                  <span class='option-title'>{{ option.title }}</span>
                  <span class='option-comment'>{{ option.comment }}</span>
                </li>
              </ul>

              <div class='quiz-correct-message' v-bind:class='{ transparent: selectedOption === null }'>
                {{ currentPage.correctMessage }}
              </div>

              <div class='girl'>
                <div class='girl-relative-container'>
                  <img src='/build/client/images/task-feminitives-girl.png'/>
                </div>
              </div>
            </div>

            <div class='page-outro' v-if='currentPage.type === "outro"'>
              <p class='outro-message'>{{ currentPage.message }}</p>

              <div class='women'>
                <img src='/build/client/images/task-feminitives-women.png'/>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class='task-button-container'>
        <div class='button' v-if='hasNextPage' v-on:click='proceedToNextPage()'>
          <span>Далі</span>
        </div>

        <div class='button' v-else v-on:click='completeTask()'>
          <span>Завершити</span>
        </div>
      </div>
    </div>
  `,

  props: [ 'completeTaskTarget' ],

  computed: {
    hasNextPage: function() {
      return this.current < this.pages.length - 1
    },

    currentPage: function() {
      return this.pages[this.current]
    },
  },

  data: function() {
    return {
      current: 0,
      selectedOption: null,
      pages: [
        {
          type: 'info',
          paragraphs: [
            'В епоху гендерної рівності необхідно завжди використовувати фемінітиви. Тим паче, що українська мова корелює з ними.',
            'Ця тема є особливо важливою, адже, наприклад, коли на співбесіді кажеш «видавчиха»  чи «редагиня», то існує ймовірність, що доведеться йти працювати до супермаркету. У кращому випадку. Або на  Facebook у палкій дискусії ви втратите перевагу, якщо поставитесь до фемінітивів зверхньо.',
            'Проаналізуємо деякі слова з видавничої справи, щоби у майбутньому не потрапити в скрутне становище.',
          ],
        },
        {
          type: 'quiz',
          text: 'Який існує коректний фемінітив до слова «видавець»?',
          correctMessage: 'Авжеж, правильна відповідь – видавчиня.',
          options: [
            {
              title: 'видавчиня',
              comment: '(звучить наче щось дворянське)',
              correct: true,
            },
            {
              title: 'видавчиха',
              comment: '(щось таке грубе, наче бабка-повитуха змінила піар-менеджера)',
              correct: false,
            },
          ],
        },
        {
          type: 'quiz',
          text: 'Доберіть фемінітив до слова «редактор»!',
          correctMessage: 'Правильна відповідь – редакторка.',
          options: [
            {
              title: 'редагиня',
              comment: '(уже не зовсім дворянське)',
              correct: false,
            },
            {
              title: 'редакторка',
              comment: '(грубувато, але звучить впевнено)',
              correct: true,
            },
          ],
        },
        {
          type: 'quiz',
          text: 'Утворіть фемінітив до слова «коректор»!',
          correctMessage: 'Правильна відповідь – коректорка.',
          options: [
            {
              title: 'коректорка',
              comment: '(знову ж таки, дуже солідно)',
              correct: true,
            },
            {
              title: 'коректор',
              comment: '(ще не придумали відповідник, це мій вибір)',
              correct: false,
            },
          ],
        },
        {
          type: 'outro',
          message:  'Будьте прогресивними, друзі, оскільки розвиток – це життя.',
        },
      ]
    }
  },

  methods: {
    quizOptionClass: function(option) {
      return {
        correct: this.selectedOption !== null && ((this.selectedOption === option && this.selectedOption.correct) || (!this.selectedOption.correct && this.selectedOption !== option)),
        incorrect: this.selectedOption !== null && this.selectedOption === option && !this.selectedOption.correct,
      }
    },

    proceedToNextPage: function() {
      if (!this.hasNextPage) { return }

      this.selectedOption = null
      this.current = this.current + 1
    },

    selectQuizOption: function(option) {
      if (this.currentPage.type !== 'quiz') { return }
      if (this.selectedOption !== null) { return }

      this.selectedOption = option
    },

    completeTask: function() {
      this.completeTaskTarget.$emit('completeTask', this)
    },

    confirmAndCloseTask: function() {
      const taskModal = this

      this.$modal.show('dialog', {
        title: 'Завершити завдання?',
        text: 'Ви впевнені, що хочете завершити проходження завдання?',
        buttons: [
          {
            title: 'Так, завершити',
            handler: function() {
              taskModal.completeTaskTarget.$emit('completeTask', taskModal)
            }
          },
          {
            title: 'Ні, продовжити',
            default: true,
            handler: function() {
              taskModal.$modal.hide('dialog')
            }
          }
        ],
      })
    },
  }
})
