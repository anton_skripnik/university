Vue.component("newcomers-task-logo", {
  template: `
    <div class='task-logo-container'>
      <div class='close-button' v-on:click='confirmAndCloseTask()'>❌</div>
      <v-dialog />
      <div class='task-container' v-if='current.displayFacts !== true'>
        <div class='task-main'>
          <div class='task-header'>
            <h2>Яке лого правильне?</h2>
          </div>
          <div class='logo-task-options-container'>
            <ul class='logo-task-options'>
              <li v-for='logo in current.challenge.logos' v-bind:class='{ incorrect: shouldHighlightAsIncorrect(logo), correct: shouldHighlightAsCorrect(logo)  }'>
                <img v-bind:src='logo.image' v-on:click='select(logo)'/>
              </li>
            </ul>
          </div>
        </div>
        <div class='logo-task-button-container' v-if='current.selection !== null'>
          <div class='button big' v-on:click='displayFactsForCurrentChallenge()'><span>Далі</span></div>
        </div>
      </div>
      <div class='facts-container' v-else>
        <div class='facts-background'>
          <div class='facts-background-book-group' v-for='group in current.challenge.facts.backgroundBookGroups'>
            <div class='facts-background-book-image' v-for='book in group'>
              <img v-bind:src='book'/>
            </div>
          </div>
        </div>
        <div class='facts-main'>
          <div class='facts-main-backdrop'>
            <div class='facts-header'>
              <h2>{{ current.challenge.facts.title ? current.challenge.facts.title : "Цікаво знати" }}</h2>
            </div>
            <div class='facts-content' v-html='current.challenge.facts.text'></div>
          </div>
        </div>
        <div class='logo-task-button-container'>
          <div class='button big' v-on:click='proceedToNextChallengeOrComplete()'><span>{{ this.factsNextButtonTitle }}</span></div>
        </div>
      </div>
    </div>
  `,

  beforeMount: function() {
    this.reset() 
  },

  props: [ 'completeTaskTarget' ],

  methods: {
    reset: function() {
      Vue.set(this, "result", [])
      Vue.set(this, "current", this.pickNext())
    },

    pickNext: function() {
      let result = null

      const seenChallenges = this.result.map(r => r.challenge).concat(this.current ? [ this.current.challenge ] : [])
      const availableChallenges = this.challenges.filter(c => seenChallenges.indexOf(c) === -1)

      if (availableChallenges.length === 0) {
        return null
      }

      let nextIndex = Math.floor(Math.random() * availableChallenges.length)

      return {
        challenge: availableChallenges[nextIndex],
        displayFacts: false,
        selection: null
      }
    },

    select: function(logo) {
      if (this.current.selection) {
        return
      }

      this.current.selection = logo
    },

    shouldHighlightAsIncorrect: function(logo) {
      if (!this.current.selection) {
        return false
      }

      return (this.current.selection.correct === false) && (this.current.selection === logo)
    },

    shouldHighlightAsCorrect: function(logo) {
      if (!this.current.selection) {
        return false
      }

      return (
        (this.current.selection.correct === true) && (this.current.selection === logo)
      || (this.current.selection.correct === false) && (this.current.selection !== logo)
      )
    },

    displayFactsForCurrentChallenge: function() {
      if (this.current) {
        this.result.push(this.current)
      }

      this.current.displayFacts = true
    },

    proceedToNextChallengeOrComplete: function() {
      const next = this.pickNext()
      Vue.set(this, 'current', next)

      if (!next) {
        this.completeTaskTarget.$emit('completeTask', this)
      }
    },

    confirmAndCloseTask: function() {
      const taskModal = this

      this.$modal.show('dialog', {
        title: 'Завершити завдання?',
        text: 'Ви впевнені, що хочете завершити проходження завдання?',
        buttons: [
          {
            title: 'Так, завершити',
            handler: function() {
              taskModal.completeTaskTarget.$emit('completeTask', taskModal)
            }
          },
          {
            title: 'Ні, продовжити',
            default: true,
            handler: function() {
              taskModal.$modal.hide('dialog')
            }
          }
        ],
      })
    },
  },

  computed: {
    hasChallengesLeft: function() {
      return (this.result.length) < this.challenges.length
    },

    factsNextButtonTitle: function() {
      if (this.hasChallengesLeft) {
        return 'Далі'
      } else {
        const totalChallenges = this.result.length
        const successes = this.result.reduce((acc, r) => acc + (r.selection.correct ? 1 : 0), 0)
        return `Завершити [Результат: ${successes}/${totalChallenges}]`
      }
    }
  },

  data: function() {
    return {
      challenges: [
        {
          logos: [
            {
              image: "/build/client/images/task-logo-11.png",
              correct: true,
            },
            {
              image: "/build/client/images/task-logo-12.png",
              correct: false,
            },
          ],
          facts: {
            title: "«А-БА-БА-ГА-ЛА-МА-ГА»",
            text: `<p>Перша «абабагаламазька» книжка «Українська абетка» побачила світ у липні 1992 року. (Перша сторінка першої книжки починалася з «Ангела»). У перекладі з дитячої мови «А-БА-БА-ГА-ЛА-МА-ГА» це, власне, і є «абетка» (так називав її герой оповідання Івана Франка «Грицева шкільна наука»).</p><p>Книжки видавництва «А−БА−БА−ГА−ЛА−МА−ГА» гідно оцінюють чимало світових та українських знаменитостей.</p><p>ПАУЛО КОЕЛЬО так відгукнувся про «Снігову Королеву», проілюстровану Владиславом Єрком: «Це чи не найдивовижніша дитяча книжка, яку я бачив у своєму житті».</p>`,
            backgroundBookGroups: [
              [
                "/build/client/images/logo-task-cover-11.jpeg",
                "/build/client/images/logo-task-cover-12.jpeg",
                "/build/client/images/logo-task-cover-13.jpeg",
              ],
              [
                "/build/client/images/logo-task-cover-14.jpeg",
                "/build/client/images/logo-task-cover-15.jpeg",
                "/build/client/images/logo-task-cover-16.jpeg",
              ]
            ],
          }
        },
        { 
          logos: [
            {
              image: "/build/client/images/task-logo-21.png",
              correct: false,
            },
            {
              image: "/build/client/images/task-logo-22.jpeg",
              correct: true,
            },
          ],
          facts: {
            title: "«Видавництво Старого Лева»",
            text: `<p>Найпрогресивніше на сьогодні видавництво України. Тут кожна книжка - must read. В асортименті є як дорослі, так і дитячі твори. Усе зроблено стильно, зі смаком, хочеться читати, читати і не зупинятися.</p><p>Одне з найпопулярніших видань в ВСЛ - розмальовка для дорослих «Чарівний сад», яка підкорила тисячі сердець.</p><p>Усі книги у видавництві - унікальні і не представлені більше ніким в Україні. Для дітей є азбука, розмальовки та казки, для дорослих − історії, романи, документальні книжки. Окрема серія є і для підлітків</p>
            <p>Must read:</p>
            <ul>
              <li>«140 децібелів тиші» − Андрій Бачинський</li>
              <li>«Схід» − Анджей Стасюк</li>
              <li>«Як зрозуміти козу» − Тарас і Мар'яна Прохасько</li>
            </ul>`,
            backgroundBookGroups: [
              [
                "/build/client/images/logo-task-cover-21.jpeg",
                "/build/client/images/logo-task-cover-22.jpeg",
                "/build/client/images/logo-task-cover-23.jpeg",
              ],
              [
                "/build/client/images/logo-task-cover-24.jpeg",
                "/build/client/images/logo-task-cover-25.jpeg",
                "/build/client/images/logo-task-cover-26.jpeg",
              ]
            ],
          }
        },
        { 
          logos: [
            {
              image: "/build/client/images/task-logo-31.jpeg",
              correct: false,
            },
            {
              image: "/build/client/images/task-logo-32.jpeg",
              correct: true,
            },
          ],
          facts: { 
            title: "«Зелений пес»",
            text: `<p>Віталій та Дмитро Капранови, засновники видавництва:</p><p>«У нас є дві версії: перша, що пес був нормальним, жовто-синім, а друкар напився і змішав фарби, от і вийшов зелений.</p><p>Друга базується на вірші Степана Руданського «Зелений пес». Зелений пес – привід для знайомства, а ми, власне, і знайомимо читачів з письменниками».</p><p>Від себе зауважимо, що перша версія дійсно життєва.</p>`,
            backgroundBookGroups: [
              [
                "/build/client/images/logo-task-cover-31.jpeg",
                "/build/client/images/logo-task-cover-32.jpeg",
                "/build/client/images/logo-task-cover-33.jpeg",
              ],
              [
                "/build/client/images/logo-task-cover-34.jpeg",
                "/build/client/images/logo-task-cover-35.jpeg",
                "/build/client/images/logo-task-cover-36.jpeg",
              ],
            ]
          }
        },
        { 
          logos: [
            {
              image: "/build/client/images/task-logo-41.jpeg",
              correct: false,
            },
            {
              image: "/build/client/images/task-logo-42.jpeg",
              correct: true,
            },
          ],
          facts: { 
            title: "Урбіно",
            text: `<p>Божена Антоняк, співзасновниця видавництва:</p><p>«Оскільки мій чоловік харків’янин, то назва Урбіно пов’язана з літературною групою Урбіно, створеною в Харкові в 1924 році. Група прибрала назву італійського міста Урбіно, розквіт якого припадає на добу кватроченто. У цьому місті народився один з найвидатніших представників Високого Відродження Рафаель Санті.</p><p>Видавництво створили для наближення українського читача до літератури європейських країн на високому перекладацькому і видавничому рівні. Таким чином ми проводимо паралель між італійським Урбіно як центром культурного Відродження і  Харковом як центром українського культурного відродження і сучасним часом, коли Україна спрямована в Європу, куди без розвиненої культури немає шляху».</p>`,
            backgroundBookGroups: [
              [
                "/build/client/images/logo-task-cover-41.png",
                "/build/client/images/logo-task-cover-42.png",
                "/build/client/images/logo-task-cover-43.png",
              ],
              [
                "/build/client/images/logo-task-cover-44.png",
                "/build/client/images/logo-task-cover-45.png",
                "/build/client/images/logo-task-cover-46.png",
              ],
            ]
          }
        },
      ],

      current: null,
      result: []
    }
  }
})
