import generatePunctuationData from './newcomers-task-punctuation-text'

function PunctuationMarkPlaceholder(originalMark) {
  this.currentAnswer = null
  this.focused = false                        // When true, should display the options popover
  this.correctAnswer = originalMark.content

  this.id = originalMark.id
  this.originalMark = originalMark

  this.correct = function() {
    if (this.currentAnswer == null || this.currentAnswer == undefined) {
      return undefined
    }

    return this.currentAnswer == this.correctAnswer
  }

  this.content = function() {
    if (this.currentAnswer) {
      return this.currentAnswer
    } else {
      return "?"
    }
  }
}

Vue.component('newcomers-task-punctuation', {
  template: `
    <div class='task-punctuation-container'>
      <div class='close-button' v-on:click='confirmAndCloseTask()'>❌</div>
      <v-dialog />

      <div class='header-container'>
        <h2>Розставте пунктуаційні знаки</h2>
      </div>
      <div class='task-container'>
        <div class='text-container'>
          <div class='paragraph' v-for='paragraph in punctuationText' :key='paragraph.id' v-on:click='dismissFocusedPlaceholderIfAny()'>
            <div v-for='word in paragraph.content' 
                 v-bind:class='[ "token", isPlaceholder(word) ? "placeholder" : "word", isPlaceholder(word) && word.correct() === true ? "correct" : "", isPlaceholder(word) && word.correct() === false ? "incorrect" : "" ]'
                 v-on:click='handleTokenClick(word, $event)'
                 :key='word.id'> 
              {{ isPlaceholder(word) ? word.content() : word.content }}

              <div v-if='isPlaceholder(word) && word.focused' class='options-popover'>
                <div class='option' v-for='mark in availablePunctuationMarks' v-on:click.stop.prevent='selectMarkForPlaceholder(mark, word)'><span>{{ mark }}</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class='task-button-container'>
          <div class='button' v-on:click='completeTask()'>
            <span>Завершити</span>
          </div>
        </div>
      </div>
    </div>
  `,
  
  data: function() {
    const punctuationData = generatePunctuationData()
    return {
      originalText: punctuationData,
      punctuationText: this.replaceHiddenElementsWithPlaceholders(punctuationData),
      availablePunctuationMarks: [ ',', ':', '–' ],
    }
  },

  props: [  'completeTaskTarget' ],

  beforeMount: function() {
    this.reset()
  },

  methods: {
    reset: function() {
    },

    isPlaceholder: function(token) {
      return token instanceof PunctuationMarkPlaceholder
    },

    handleTokenClick: function(token, event) {
      if (!this.isPlaceholder(token)) {
        return
      }

      event.preventDefault()
      event.stopPropagation()

      this.dismissFocusedPlaceholderIfAny()

      token.focused = true
    },

    selectMarkForPlaceholder: function(mark, placeholder) {
      if (!this.isPlaceholder(placeholder)) {
        return
      }

      placeholder.currentAnswer = mark

      placeholder.focused = false
    },

    dismissFocusedPlaceholderIfAny: function() {
      this.punctuationText.
        forEach(paragraph => {
          paragraph.content
            .filter(x => (x instanceof PunctuationMarkPlaceholder && x.focused))
            .forEach(x => x.focused = false)
        })
    },

    replaceHiddenElementsWithPlaceholders: function(punctuationData) {
      let updatedData = []

      punctuationData.forEach(function(paragraph) {
        let updatedParagraph = {
          type: paragraph.type,
          id: paragraph.id,
          content: []
        }

        paragraph.content.forEach(function(word) {
          if (!word.hide) {
            updatedParagraph.content.push(word)
          } else {
            updatedParagraph.content.push(new PunctuationMarkPlaceholder(word))
          }
        })

        updatedData.push(updatedParagraph)
      })

      return updatedData
    },

    completeTask: function() {
      this.completeTaskTarget.$emit('completeTask', this)
    },

    confirmAndCloseTask: function() {
      const taskModal = this

      this.$modal.show('dialog', {
        title: 'Завершити завдання?',
        text: 'Ви впевнені, що хочете завершити проходження завдання?',
        buttons: [
          {
            title: 'Так, завершити',
            handler: function() {
              taskModal.completeTaskTarget.$emit('completeTask', taskModal)
            }
          },
          {
            title: 'Ні, продовжити',
            default: true,
            handler: function() {
              taskModal.$modal.hide('dialog')
            }
          }
        ],
      })
    },

  },
})
