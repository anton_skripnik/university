const { v4: uuidv4 } = require('uuid')

Vue.component('newcomers-task-rhethoric-hints', {
  template:`
    <div class='task-rhethoric-hints-container'>
      <div class='close-button' v-on:click='confirmAndCloseTask()'>❌</div>
      <v-dialog />

      <div class='task-container'>
        <div class='task-main' ref=taskMain>

          <div class='hints-page' v-if='currentPage.type === "hints"'>
            <div class='section' v-for='section in currentPage.sections'>
              <div class='image-container'>
                <img v-bind:src='section.image'/>
              </div>
              <div class='header-container'>
                <h2>{{ section.title }}</h2>
              </div>
              <div class='hint-container'>
                {{ section.text }}
              </div>
            </div>
          </div>

          <div class='synonyms-page' v-else-if='currentPage.type === "synonyms"'>
            <div class='header'>
              <h2>{{ currentPage.title }}</h2>
            </div>

            <div class='synonym' v-for='synonym in currentPage.synonyms'>
              <div class='example' v-html='synonym.example'></div>
              <div class='options'>
                <div class='option'
                     v-for='option in synonym.options' @click='select(option, synonym)'>
                  <input type='radio' 
                         :id='option.id'
                         :name='synonym.id'
                         :disabled='synonym.selection'
                         :checked='option === synonym.selection'/>
                  <label :for='option.id' :class='classForItemBasedOnSelectionState(option, synonym)'>{{ option.word }}</label>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class='task-button-container'>
        <div class='button' 
             @click='proceedToSynonyms()'
             v-if='currentPage.type === "hints"'>
          <span>Далі</span>
        </div>
        <div class='button' 
             @click='completeTask()'
             v-else>
          <span>Завершити</span>
        </div>
      </div>
    </div>
  `,

  props: [ 'completeTaskTarget' ],

  computed: {
    currentPage: function() {
      return this.pages[this.currentPageIdx]
    },
  },

  data: function() {
    return {
      currentPageIdx: 0,
      pages: [
        {
          type: "hints",
          sections: [
            {
              title: "Виступ на тему",
              image: "/build/client/images/task-rhethorics-1.png",
              text: "Оберіть будь-яку тему (для початку краще взяти знайому) і протягом 3-5 хвилин говоріть винятково на цю тему. Говорити, в ідеалі, потрібно зв'язно, впевнено, красиво, без затримок і використання слів-паразитів. Ця вправа змусить Ваш мозок працювати швидше. Спочатку буде складно вичавити з себе хоча б два речення, але згодом Ваша думка розвиватиметься, хоча логіка сказаного буде сумнівною. Виконання подібної вправи необхідно довести до такого рівня, щоб у Вашому монолозі не було прогалин і явних огріхів."
            },
            {
              title: "Словник + Ви",
              image: "/build/client/images/task-rhethorics-2.png",
              text: "Уявіть ситуацію, коли Вам запропонували стати співавтором тлумачного словника. Ваше завдання − дати точне і ґрунтовне визначення слів. Спочатку візьміть прості слова, потім слід перейти до абстрактних понять. Нехай Ваше визначення не обмежується парою речень, постарайтеся дати визначення слову таким чином, щоб сплутати його з іншим було нереально. Розвивайте свою думку і не обмежуйтеся одним реченням. Наприклад, дайте визначення таким словам, як креативність, риторика."
            },
            {
              title: "У пошуках прикметників",
              image: "/build/client/images/task-rhethorics-3.png",
              text: "Беремо будь-яке слово і пишемо 5 прикметників, які підходять до нього за змістом. Наприклад, «абітурієнт(ка) ЗНУ» − розумний(а) (бо обрав(ла) правильний заклад), толерантний(а), кмітливий(а), талановитий(а), працьовитий(а). Потім напишіть 5 прикметників, які жодним чином не корелюють з ним. Наприклад, той же «абітурієнт(ка) ЗНУ» − понурий(а), багряний(а), дерев'яний(а), олов'яний(а), бездарний(а). Виконуючи вправу, Ви помітите, що не так легко дістати зі своєї пам'яті задані прикметники."
            },
          ],
        },
        {
          type: "synonyms",
          title: "Доберіть найвдаліший відповідник/синонім та подумки вставте його на місце канонічного, щоб контекст не втратив сенс!",
          synonyms: [
            {
              id: uuidv4(),
              originalWord: "Чудернацький",
              options: [
                {
                  id: uuidv4(),
                  word: "Оригінальний",
                  correct: false,
                },
                {
                  id: uuidv4(),
                  word: "Дивний",
                  correct: true,
                },
                {
                  id: uuidv4(),
                  word: "Знайомий",
                  correct: false,
                }
              ],
              example: "З Зоєю прийшов Кость, який мав <span class='emphasis'>чудернацьке</span> прізвище - Закопайвухо <span class='source'>(О. Донченко)</span>.",
              selection: null,
            },
            {
              id: uuidv4(),
              originalWord: "Кватирка",
              options: [
                {
                  id: uuidv4(),
                  word: "Кімната",
                  correct: false,
                },
                {
                  id: uuidv4(),
                  word: "Віконце",
                  correct: true,
                },
                {
                  id: uuidv4(),
                  word: "Каміно",
                  correct: false,
                }
              ],
              example: "В глибині перистиля вузька брама з <span class='emphasis'>кватиркою</span> в одній половинці й з хвірткою в другій <span class='source'>(Леся Українка, III, 1952, 270)</span>.",
              selection: null,
            },
            {
              id: uuidv4(),
              originalWord: "Докóнче",
              options: [
                {
                  id: uuidv4(),
                  word: "Скоро",
                  correct: false,
                },
                {
                  id: uuidv4(),
                  word: "Зараз",
                  correct: false,
                },
                {
                  id: uuidv4(),
                  word: "Обов'язково",
                  correct: true,
                }
              ],
              example: "Переляканий батько ледве переконав його, що вчитись <span class='emphasis'>доконче</span> треба <span class='source'>(Володимир Гжицький, У світ.., 1960, 10)</span>.",
              selection: null,
            },
            {
              id: uuidv4(),
              originalWord: "Зáкутень",
              options: [
                {
                  id: uuidv4(),
                  word: "Глухе місце",
                  correct: true,
                },
                {
                  id: uuidv4(),
                  word: "Територія",
                  correct: false,
                },
                {
                  id: uuidv4(),
                  word: "Темрява",
                  correct: false,
                }
              ],
              example: "Село наше у <span class='emphasis'>закутні</span> такому, що ніхто туди не зайде <span class='source'>(Словник Грінченка)</span>.",
              selection: null,
            },
            {
              id: uuidv4(),
              originalWord: "Скалка",
              options: [
                {
                  id: uuidv4(),
                  word: "Уламок",
                  correct: true,
                },
                {
                  id: uuidv4(),
                  word: "Качалка для тіста",
                  correct: false,
                },
                {
                  id: uuidv4(),
                  word: "Скеля",
                  correct: false,
                }
              ],
              example: "На лобі глибока рана: різонуло, мабуть, великою <span class='emphasis'>скалкою</span> розбитої шибки <span class='source'>(Юрій Шовкопляс, Інженери, 1956, 350)</span>.",
              selection: null,
            },
          ]
        }
      ]
    }
  },

  beforeMount: function() {
    this.reset()
  },

  methods: {
    classForItemBasedOnSelectionState: function(item, synonym) {
      if (!synonym.selection) { return '' }
      if (item !== synonym.selection) { 
        return item.correct ? 'correct-option' : ''
      }

      return item.correct ? 'correct-option' : 'incorrect-option'
    },

    select: function(item, synonym) {
      if (synonym.selection) { return }

      synonym.selection = item
    },

    proceedToSynonyms: function() {
      if (this.currentPage.type !== "hints") { return }

      this.currentPageIdx = 1

      this.$refs.taskMain.scroll(0, 0)
    },

    completeTask: function() {
      this.completeTaskTarget.$emit('completeTask', this)
    },

    confirmAndCloseTask: function() {
      const taskModal = this

      this.$modal.show('dialog', {
        title: 'Завершити завдання?',
        text: 'Ви впевнені, що хочете завершити проходження завдання?',
        buttons: [
          {
            title: 'Так, завершити',
            handler: function() {
              taskModal.completeTaskTarget.$emit('completeTask', taskModal)
            }
          },
          {
            title: 'Ні, продовжити',
            default: true,
            handler: function() {
              taskModal.$modal.hide('dialog')
            }
          }
        ],
      })
    },
  },

})
