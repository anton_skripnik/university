Vue.component('newcomers-task-vocabulary', {
  template: `
    <div class='task-vocabulary-container'>
      <div class='close-button' v-on:click='confirmAndCloseTask()'>❌</div>
      <v-dialog />

      <div class='task-container'>
        <div class='background-decoration'>
          <div class='left-side-decoration'>
            <img src='/build/client/images/task-background-side-1.png'/>
          </div>

          <div class='right-side-decoration'>
            <img src='/build/client/images/task-background-side-2.png'/>
          </div>
        </div>

        <div class='task-main'>
          <div class='task-question'>
            <h2>{{ current.challenge.question }}</h2>
          </div>

          <div class='task-options-container'>
            <ul class='task-options'>
              <li v-for='option in current.challenge.options' 
                  v-bind:class='{ incorrect: shouldHighlightAsIncorrect(option), correct: shouldHighlightAsCorrect(option) }'
                  v-on:click='select(option)'>
                <div class='option-details' v-if='shouldShowDetails(option)'><div v-html='option.details'></div></div>
                <div class='option-text' v-else>{{ option.text }}</div>
              </li>
            </ul>
          </div>

          <div class='small-screen-decoration-container'>
            <div class='decoration'>
              <img class='typewriter' src='/build/client/images/task-background-typewriter.png'/>
            </div>
            <div class='decoration'>
              <img class='coffee' src='/build/client/images/task-background-coffee.png'/>
            </div>
          </div>

          <div class='task-button-container' v-bind:class='{ transparent: current.selection === null }'>
            <div class='button big' v-on:click='proceedToNextChallengeOrComplete()'><span>{{ nextButtonTitle }}</span></div>
          </div>
        </div>
      </div>
    </div>
  `,
  
  computed: {
    hasChallengesLeft: function() {
      return (this.result.length) < this.challenges.length
    },

    nextButtonTitle: function() {
      if (this.hasChallengesLeft) {
        return 'Далі'
      } else {
        const totalChallenges = this.result.length
        const successes = this.result.reduce((acc, r) => acc + (r.selection.correct ? 1 : 0), 0)
        return `Завершити [Результат: ${successes}/${totalChallenges}]`
      }
    },
  },

  data: function() {
    return {
      result: [],
      current: null,
      challenges: [
        {
          question: "Без чого неможливо в суцільній темряві добре побачити певні речі?",
          options: [
            {
              text: "Ліхтарик",
              correct: true,
              details: "<strong>Ліхтарик</strong> &mdash; підзаголовок, набраний у кілька рядків вузьким форматом (від півтора до двох квадратів) і зверстаний на внутрішньому або зовнішньому полі сторінки поза текстом шпальти. Набирається в оборку іншими шрифтами (курсивним, жирним тощо). Іноді береться в рамочку з лінійок."
            },
            {
              text: "Запальничка",
              correct: false,
            },
            {
              text: "Проміння",
              correct: false,
            }
          ]
        },
        {
          question: "Куди страшно заходити без ліхтарика і де найчастіше у фільмах жахів трапляються моторошні персонажі?",
          options: [
            {
              text: "Горище",
              correct: false,
            },
            {
              text: "Підвал",
              correct: true,
              details: "<strong>Підвал</strong> &mdash; місце внизу газетної сторінки, де розташовано текст, що займає всю ширину сторінки чи більшу частину її шпальт.",
            },
            {
              text: "Капітель",
              correct: false,
            }
          ]
        },
        {
          question: "Термін, що описує радісне ставлення до різних ситуацій; це слово взяв собі нікнеймом український артист, дует якого нещодавно розпався.",
          options: [
            {
              text: "Каптал",
              correct: false,
            },
            {
              text: "Негатив",
              correct: false,
            },
            {
              text: "Позитив",
              correct: true,
              details: "<strong>Позитив</strong> &mdash; зображення, ідентичне за градаційними параметрами оригіналу, виготовлене на непрозорій основі."
            }
          ]
        },
        {
          question: "Яке слово вважається помилко-небезпечним місцем у поліграфії? Підказка:  студенти часто-густо називають це слово – полосою.",
          options: [
            {
              text: "Пагінація",
              correct: false,
            },
            {
              text: "Шапка",
              correct: false,
            },
            {
              text: "Шпальта",
              correct: true,
              details: "<strong>Шпальта</strong> &mdash; частина сторінки видання, відокремлена по вертикалі проміжками чи лініями."
            }
          ]
        },
      ],
    }
  },

  props: [  'completeTaskTarget' ],
 
  beforeMount: function() {
    this.reset()
  },

  methods: {
    reset: function() {
      Vue.set(this, "result", [])
      Vue.set(this, "current", this.pickNext())
    },

    pickNext: function() {
      let result = null

      const seenChallenges = this.result.map(r => r.challenge).concat(this.current ? [ this.current.challenge ] : [])
      const availableChallenges = this.challenges.filter(c => seenChallenges.indexOf(c) === -1)

      if (availableChallenges.length === 0) {
        return null
      }

      let nextIndex = Math.floor(Math.random() * availableChallenges.length)

      return {
        challenge: availableChallenges[nextIndex],
        selection: null
      }
    },

    shouldHighlightAsIncorrect: function(option) {
      return this.current.selection && option === this.current.selection && !option.correct
    },

    shouldHighlightAsCorrect: function(option) {
      return this.current.selection && option.correct
    },

    shouldShowDetails: function(option) {
      return (this.current.selection !== null && this.current.selection !== undefined) && option.correct
    },

    select: function(option) {
      if (this.current.selection) {
        return
      }

      this.current.selection = option

      this.result.push(this.current)
    },

    proceedToNextChallengeOrComplete: function() {
      const next = this.pickNext()
      Vue.set(this, 'current', next)

      if (!next) {
        this.completeTaskTarget.$emit('completeTask', this)
      }
    },

    confirmAndCloseTask: function() {
      const taskModal = this

      this.$modal.show('dialog', {
        title: 'Завершити завдання?',
        text: 'Ви впевнені, що хочете завершити проходження завдання?',
        buttons: [
          {
            title: 'Так, завершити',
            handler: function() {
              taskModal.completeTaskTarget.$emit('completeTask', taskModal)
            }
          },
          {
            title: 'Ні, продовжити',
            default: true,
            handler: function() {
              taskModal.$modal.hide('dialog')
            }
          }
        ],
      })
    },
  },
})
