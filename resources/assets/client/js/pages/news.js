import {http} from "../http";

Vue.component('news', {

    data() {
        return {
            news: [{
                id: '',
                title: '',
                description: '',
                image: '',
                slug: '',
            }],
            page: {current_page: 1, total: 1, per_page: 1},
        }
    },

    created() {
        http.get(window.laroute.route('news.showMore'))
            .then(response => {
                this.news = response.data[0];
                this.page = response.data[1];
            })
            .catch(err => console.log(err))
    },

    methods: {
        fetchData(page) {

            page = page || 1;
            http.get(window.laroute.route('news.showMore') + '?page=' + page)
                .then(response => {
                    for(let i = 0; i < response.data[0].length; i++) {
                        this.news.push(response.data[0][i])
                    }
                    this.page.current_page = page
                })
                .catch(err => console.log(err))
        },
        goToPage(slug) {
            window.location.href = window.laroute.route('news.show', {news: slug});
        },
    },
});