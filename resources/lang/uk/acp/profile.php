<?php

declare(strict_types=1);

return [
    'main' => [
        'title' => 'Профіль',
    ],

    'edit' => [
        'form' => [
            'fieldsets' => [
                'title' => 'Редагувати профіль',
            ],
            'fields'    => [
                'name'                  => [
                    'label'  => 'Ім\'я',
                    'errors' => [
                        'required' => 'Обовʼязкове поле',
                        'string'   => 'Імʼя повинно бути рядком',
                        'min'      => 'Імʼя повинно мати більше, ніж :min символи',
                        'max'      => 'Імʼя не повинно мати більше, ніж :max символів',
                    ],
                ],
                'email'                 => [
                    'label'  => 'Email',
                    'errors' => [
                        'required' => 'Обовʼязкове поле',
                        'email'    => 'Введіть валідний email',
                        'unique'   => 'Введений email вже існує',
                    ],
                ],
                'password'              => [
                    'label'  => 'Пароль',
                    'errors' => [
                        'min'       => 'Пароль повинен мати більше, ніж :min символів',
                        'confirmed' => 'Пароль та підтвердження не збігаються',
                    ],
                ],
                'password_confirmation' => [
                    'label' => 'Підтвердження паролю',
                ],
            ],
            'buttons'   => [
                'submit' => [
                    'text' => 'Зберегти',
                ],
            ],
            'messages'  => [
                'success' => 'Профіль успішно відредаговано',
                'failure' => 'Сталася помилка під час оновлення',
            ],
        ],
    ],
];
