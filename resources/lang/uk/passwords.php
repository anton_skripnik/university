<?php

declare(strict_types = 1);

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль повинен містити принаймні шість символів і відповідати підтвердженню.',
    'reset'    => 'Ваш пароль був скинутий!',
    'sent'     => 'We have e-mailed your password reset link!',
    'token'    => 'Ми надіслали вам електронний лист із посиланням на скидання пароля!',
    'user'     => "Ми не можемо знайти користувача з цією адресою електронної пошти.",

];
