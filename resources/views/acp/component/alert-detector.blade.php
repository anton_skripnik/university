@php
    $close = $close ?? \true;
@endphp

@includeWhen(Session::has('success'), 'acp.component._alert-detector.success', [
    'close'     => $close,
    'alertText' => Session::get('success')
])

@includeWhen(Session::has('info'), 'acp.component._alert-detector.info', [
   'close'     => $close,
    'alertText' => Session::get('info')
])

@includeWhen(Session::has('warning'), 'acp.component._alert-detector.warning', [
    'close'     => $close,
    'alertText' => Session::get('warning')
])

@includeWhen(Session::has('error'), 'acp.component._alert-detector.danger', [
    'close'     => $close,
    'alertText' => Session::get('error')
])

