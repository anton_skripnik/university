@php
    switch ($severity) {
        case 'success':
            $icon = 'fa-check';
            break;
        case 'info':
            $icon = 'fa-info-circle';
            break;
        case 'warning':
            $icon = 'fa-warning';
            break;
        case 'danger':
            $icon = 'fa-times-circle';
            break;
    }
@endphp

<div class="alert alert-{{ $severity }} alert-white rounded">
    <button type="button" class="close" data-dismiss="alert">
        <span>&times;</span>
        <span class="sr-only">@lang('generic.word.close')</span>
    </button>

    <div class="icon">
        <i class="fa {{ $icon }}"></i>
    </div>

    {{ $slot }}
</div>
