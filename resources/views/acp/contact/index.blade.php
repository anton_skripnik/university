@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/contact.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.contact.update', $contacts->id) !!}" method="post" role="form"
                  class="col-sm-8 col-sm-offset-2">
                {!! csrf_field() !!}
                {!! method_field('patch') !!}
                <legend>@lang('acp/contact.edit.form.fieldsets.title')</legend>

                <div class="form-group required {{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="control-label" for="address">
                        @lang('acp/contact.edit.form.fields.address.label')</label>
                    <textarea class="form-control markdown_editor" name="address" id="address"
                              placeholder="@lang('acp/contact.edit.form.fields.address.label')">
                        {{ $contacts->address }}
                    </textarea>
                    @includeWhen($errors->has('address'), 'acp.component.field-error', ['field' => 'address'])
                </div>

                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label class="control-label" for="phone">@lang('acp/contact.edit.form.fields.phone.label')</label>
                    <input value="{{ $contacts->phone }}" type="text" class="form-control" name="phone"
                           id="phone" placeholder="@lang('acp/contact.edit.form.fields.phone.label')">
                    @includeWhen($errors->has('phone'), 'acp.component.field-error', ['field' => 'phone'])
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/contact.edit.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
