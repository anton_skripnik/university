@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/gallery.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.gallery.store') !!}" method="post" role="form" enctype="multipart/form-data"
                  class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/gallery.create.form.fieldsets.title')</legend>

                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="control-label" for="photo_title">
                        @lang('acp/gallery.create.form.fields.title.label')
                    </label>
                    <input required type="text" class="form-control"
                           name="title" id="photo_title" value="{{ old('title') }}"
                           placeholder="@lang('acp/gallery.create.form.fields.title.label')">
                    @includeWhen($errors->has('title'), 'acp.component.field-error', ['field' => 'title'])
                </div>

                <div class="form-group required {{ $errors->has('photo') ? ' has-error' : '' }}">
                    <label class="control-label" for="photo">
                        @lang('acp/gallery.create.form.fields.photo.label')
                    </label>
                    <input type="file" accept="image/jpeg,image/png" class="form-control" name="photo" id="photo">
                    @includeWhen($errors->has('photo'), 'acp.component.field-error', ['field' => 'photo'])
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/gallery.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
