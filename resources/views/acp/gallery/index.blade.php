@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/gallery.main.title')</strong></h4>
        </div>
    </div>
    <div class="photo-panel acp-panel">
    @forelse($photos as $photo)
        <div id="photo_{{ $photo->getKey() }}" class="panel panel-default col-md-3">
            <div class="panel-body photo-block">
                <div>
                    <img src="{{ $photo->getPhotoUrlAttribute('sm') }}" alt="">
                </div>
                <p class="text-center">{{ $photo->title }}</p>
            </div>
            <hr>
            <span class="text-center">
                <a href="{!! route('acp.gallery.edit', $photo->getKey()) !!}"><i class="fa fa-pencil"></i></a>
                <delete url="{!! route('acp.gallery.destroy', $photo->getKey()) !!}"
                        remove="#photo_{{ $photo->getKey() }}"></delete>
            </span>
        </div>
    @empty
    @endforelse
    </div>
    <div class="col-md-12">
        {{ $photos->links() }}
    </div>
@endsection
