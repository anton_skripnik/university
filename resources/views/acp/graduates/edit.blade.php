@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/graduate.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.graduate.update', $graduate->getKey()) !!}" method="post"
                  enctype="multipart/form-data" role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/graduate.edit.form.fieldsets.title')</legend>

                {!! method_field('patch') !!}
                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label" for="name">@lang('acp/graduate.edit.form.fields.name.label')</label>
                    <input required value="{{$graduate->name}}" type="text" class="form-control" name="name"
                           id="name" placeholder="@lang('acp/graduate.edit.form.fields.name.label')">
                    @includeWhen($errors->has('name'), 'acp.component.field-error', ['field' => 'name'])
                </div>

                <div class="form-group required {{ $errors->has('subheader') ? ' has-error' : '' }}">
                    <label class="control-label" for="subheader">
                        @lang('acp/graduate.edit.form.fields.subheader.label')
                    </label>
                    <input required value="{{ $graduate->subheader }}" type="text" class="form-control"
                           name="subheader" id="subheader">
                    @includeWhen($errors->has('subheader'), 'acp.component.field-error', ['field' => 'subheader'])
                </div>

                <div class="form-group required {{ $errors->has('text') ? ' has-error' : '' }}">
                    <label class="control-label" for="text">
                        @lang('acp/graduate.edit.form.fields.biography.label')
                    </label>
                    <textarea class="form-control markdown_editor" name="text" id="text"
                              placeholder="@lang('acp/graduate.edit.form.fields.biography.label')">
                        {{ $graduate->text }}
                    </textarea>
                    @includeWhen($errors->has('text'), 'acp.component.field-error', ['field' => 'text'])
                </div>

                <div class="form-group required {{ $errors->has('avatar') ? ' has-error' : '' }}">
                    <label class="control-label" for="avatar">
                        @lang('acp/graduate.edit.form.fields.avatar.label')
                    </label>
                    <input type="file" accept="image/jpeg,image/png" class="form-control" name="avatar" id="avatar">
                    @includeWhen($errors->has('avatar'), 'acp.component.field-error', ['field' => 'avatar'])
                </div>

                <img src="{{ $graduate->getPhotoUrlAttribute('thumb') }}" alt="">

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/graduate.edit.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
