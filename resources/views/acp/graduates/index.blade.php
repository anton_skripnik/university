@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/graduate.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>@lang('acp/graduate.main.table.name')</th>
                    <th class="text-center">@lang('acp/graduate.main.table.edit')</th>
                    <th class="text-center">@lang('acp/graduate.main.table.delete')</th>
                    <th class="text-center">@lang('acp/graduate.main.table.move')</th>
                </tr>
                </thead>
                <tbody id="graduate-sort">
                @forelse($graduates as $graduate)
                    <tr id="graduate_{{ $graduate->getKey()}}" data-id="{{$graduate->getKey() }}">
                        <td>
                            <a href="{!! route('acp.graduate.show', $graduate->getKey()) !!}">
                                {{ $graduate->name }}
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{!! route('acp.graduate.edit', $graduate->getKey()) !!}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                        <td class="text-center"><delete url="{!! route('acp.graduate.destroy', $graduate->getKey()) !!}"
                                                        remove="#graduate_{{ $graduate->getKey() }}"></delete></td>
                        <td class="text-center">
                            <a href="#">
                                <i class="fa fa-arrows handle" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="4">Немає випускників</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
