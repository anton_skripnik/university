@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/history.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form enctype="multipart/form-data" action="{!! route('acp.history.update', $history->getKey()) !!}"
                  method="post" role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/history.edit.form.fieldsets.title')</legend>

                {!! method_field('patch') !!}
                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="control-label" for="history_title">
                        @lang('acp/history.create.form.fields.title.label')
                    </label>
                    <input required value="{{$history->title}}" type="text" class="form-control"
                           name="title" id="history_title"
                           placeholder="@lang('acp/history.create.form.fields.title.label')">
                    @includeWhen($errors->has('title'), 'acp.component.field-error', ['field' => 'title'])
                </div>

                <div class="form-group required {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="control-label" for="history_description">
                        @lang('acp/history.create.form.fields.description.label')
                    </label>
                    <textarea class="form-control editor markdown_editor"
                              name="description" id="history_description"
                              placeholder="@lang('acp/history.create.form.fields.description.label')">
                        {{$history->description}}
                    </textarea>
                    @includeWhen($errors->has('description'), 'acp.component.field-error', ['field' => 'description'])
                </div>

                <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}">
                    <label class="control-label" for="picture">
                        @lang('acp/news.create.form.fields.picture.label')
                    </label>
                    <input type="file" multiple accept="image/jpeg,image/png"
                           class="form-control" name="picture[]" id="picture">
                    @includeWhen($errors->has('picture'), 'acp.component.field-error', ['field' => 'picture'])
                </div>

                <div class="news_image_block edit col-md-12">
                    @forelse($history->getMedia('aboutus') as $pic)
                        <div id="history_{{ $pic->getKey() }}">
                            <img src="{{$pic->getUrl('sm')}}" alt="">
                            <delete url="{!! route('acp.history.pic.destroy', $pic->getKey()) !!}"
                                    remove="#history_{{ $pic->getKey() }}"></delete>
                        </div>
                    @empty
                    @endforelse
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/history.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
