@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        @if(!\blank($history))
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{ $history->title }}</strong></h4>
        </div>
        <div class="panel-body">
            <div class="col-md-10 col-md-offset-1 main_info_block">
                {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($history->description) !!}
            </div>
            <div class="news_image_block col-md-12">
                @forelse($history->getMedia('aboutus') as $pic)
                    <div><img src="{{ $pic->getUrl('sm') }}" alt=""></div>
                @empty
                @endforelse
            </div>
        </div>
        <div class="panel-footer text-right">
            <a href="{!! route('acp.history.edit', $history->getKey()) !!}"
               class="btn btn-primary">Редагувати
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        @else
            <div class="panel-heading acp-panel-heading">
                <h4 class=""><strong>@lang('acp/history.main.title')</strong></h4>
                <a href="{!! route('acp.history.create') !!}" type="button" class="btn btn-primary">
                    Створити історію
                </a>
            </div>
        @endif
    </div>
@endsection
