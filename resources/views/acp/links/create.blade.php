@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/link.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.link.store') !!}" method="post"
                  enctype="multipart/form-data" role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/link.create.form.fieldsets.title')</legend>

                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="control-label" for="link_title">
                        @lang('acp/link.create.form.fields.title.label')
                    </label>
                    <input required type="text" class="form-control"
                           name="title" id="link_title" value="{{ old('title') }}"
                           placeholder="@lang('acp/link.create.form.fields.title.label')">
                    @includeWhen($errors->has('title'), 'acp.component.field-error', ['field' => 'title'])
                </div>

                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="control-label" for="description">
                        @lang('acp/link.create.form.fields.link.label')
                    </label>
                    <textarea class="form-control markdown_editor" name="description" id="description"
                              placeholder="@lang('acp/link.create.form.fields.link.label')"
                    >{{ old('description') }}</textarea>
                    @includeWhen($errors->has('description'), 'acp.component.field-error', ['field' => 'description'])
                </div>

                <div class="form-group required {{ $errors->has('category') ? ' has-error' : '' }}">
                    <label class="control-label" for="category-select">
                        @lang('acp/link.create.form.fields.category.label')
                    </label>
                    <select name="category" id="category-select" class="form-control">
                        @forelse($categories as $category)
                            <option value="{{$category}}">{{$category}}</option>
                        @empty
                        @endforelse
                    </select>
                    @includeWhen($errors->has('category'), 'acp.component.field-error', ['field' => 'link'])
                </div>

                <div class="form-group {{ $errors->has('info') ? ' has-error' : '' }}">
                    <label class="control-label" for="info">@lang('acp/gallery.create.form.fields.photo.label')</label>
                    <input type="file" multiple class="form-control" name="info[]" id="info">
                    @includeWhen($errors->has('info'), 'acp.component.field-error', ['field' => 'info'])
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/link.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
