@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/link.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.link.update', $item->getKey()) !!}" method="post"
                  enctype="multipart/form-data" role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/link.edit.form.fieldsets.title')</legend>

                {!! method_field('patch') !!}
                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="control-label" for="link_title">
                        @lang('acp/link.create.form.fields.title.label')
                    </label>
                    <input required type="text" class="form-control" value="{{ $item->title }}"
                           name="title" id="link_title" placeholder="@lang('acp/link.create.form.fields.title.label')">
                    @includeWhen($errors->has('title'), 'acp.component.field-error', ['field' => 'title'])
                </div>

                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="control-label" for="description">
                        @lang('acp/link.create.form.fields.link.label')
                    </label>
                    <textarea class="form-control markdown_editor" name="description" id="description"
                              placeholder="@lang('acp/link.create.form.fields.link.label')">
                        {{ $item->description }}
                    </textarea>
                    @includeWhen($errors->has('description'), 'acp.component.field-error', ['field' => 'description'])
                </div>

                <div class="form-group required {{ $errors->has('category') ? ' has-error' : '' }}">
                    <label class="control-label" for="category-select">
                        @lang('acp/link.create.form.fields.category.label')
                    </label>
                    <select name="category" id="category-select" class="form-control">
                        @forelse($categories as $key => $category)
                            <option @if($item->getAttribute('category')->getKey() === $key) @endif
                                    value="{{$category}}">{{$category}}</option>
                        @empty
                        @endforelse
                    </select>
                    @includeWhen($errors->has('category'), 'acp.component.field-error', ['field' => 'link'])
                </div>

                <div class="form-group {{ $errors->has('info') ? ' has-error' : '' }}">
                    <label class="control-label" for="info">@lang('acp/gallery.create.form.fields.photo.label')</label>
                    <input type="file" multiple class="form-control" name="info[]" id="info">
                    @includeWhen($errors->has('info'), 'acp.component.field-error', ['field' => 'info'])
                </div>

                <div class="news_image_block file_image_block edit col-md-12">
                    @foreach($item->getMedia('info_pic') as $pic)
                        <div id="links_{{ $pic->getKey() }}">
                            <img src="{{$pic->getUrl('sm')}}" alt="">
                            <delete url="{!! route('acp.link.pic.destroy', $pic->getKey()) !!}"
                                    remove="#links_{{ $pic->getKey() }}"></delete>
                        </div>
                    @endforeach
                    @foreach($item->getMedia('info_file') as $pic)
                        <div class="link_file" id="links_{{ $pic->getKey() }}">
                            <a class="open_link" target="_blank" href="{{ $pic->getUrl() }}">
                                <p>
                                    <i class="fa fa-file fa-3x"></i>
                                    {{ $pic->getAttribute('name') }}
                                </p>
                            </a>
                            <delete class="link_delete" url="{!! route('acp.link.pic.destroy', $pic->getKey()) !!}"
                                    remove="#links_{{ $pic->getKey() }}"></delete>
                        </div>
                    @endforeach
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/link.edit.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
