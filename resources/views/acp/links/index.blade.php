@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/news.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>@lang('acp/link.main.table.name')</th>
                    <th class="text-center">@lang('acp/link.main.table.category')</th>
                    <th class="text-center">@lang('acp/link.main.table.edit')</th>
                    <th class="text-center">@lang('acp/link.main.table.delete')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($links as $item)
                    <tr id="news_{{$item->getKey()}}">
                        <td><a href="{!! route('acp.link.show', $item->id) !!}">{{$item->title}}</a></td>
                        <td class="text-center">{{$item->category->category}}</td>
                        <td class="text-center">
                            <a href="{!! route('acp.link.edit', $item->getKey()) !!}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                        <td class="text-center"><delete url="{!! route('acp.link.destroy', $item->getKey()) !!}"
                                                        remove="#news_{{ $item->getKey() }}"></delete></td>
                    </tr>
                @empty
                    <tr>
                        <td>Ще немає посилань</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $links->links() }}
        </div>
    </div>
@endsection
