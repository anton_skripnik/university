@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{ $item->getAttribute('category')->getAttribute('category') }}</strong></h4>
        </div>
        <div class="panel-body">
            <h4>{{ $item->getAttribute('title') }}</h4>
            <div class="col-md-offset-1 col-md-10" data-provide="markdown-editable">
                {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($item->description) !!}
            </div>
            <div class="news_image_block col-md-12">
                @foreach($item->getMedia('info_pic') as $pic)
                    <div><img src="{{$pic->getUrl('sm')}}" alt=""></div>
                @endforeach
                @foreach($item->getMedia('info_file') as $pic)
                    <div>
                        <a href="{{ $pic->getUrl() }}">
                            <p>
                                <i class="fa fa-file"></i>
                                {{ $pic->getAttribute('name') }}
                            </p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="panel-footer text-right">
            <a href="{!! route('acp.link.edit', $item->getKey()) !!}" class="btn btn-primary">
                Редагувати
                <i class="fa fa-pencil"></i>
            </a>
        </div>
    </div>
@endsection
