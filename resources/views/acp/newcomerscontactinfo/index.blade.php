@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/newcomerscontactinfo.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <th>@lang('acp/newcomerscontactinfo.main.table.email')</th>
                    <th class="text-center">@lang('acp/newcomerscontactinfo.main.table.cell')</th>
                    <th class="text-center">@lang('acp/newcomerscontactinfo.main.table.school')</th>
                </thead> 
                <tbody>
                @forelse($newcomerscontactinfo as $contact)
                  <tr>
                      <td><a href="mailto:{!! $contact->email !!}">{{ $contact->email }}</a></td>
                      <td class="text-center"><a href="tel:{!! $contact->cell !!}">{{ $contact->cell }}</a></td>
                      <td class="text-center">{{ $contact->school }}</td>
                  </tr>
                @empty
                  <tr>
                      <td>@lang('acp/newcomerscontactinfo.main.noentries')</td>
                      <td></td>
                      <td></td>
                  </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
