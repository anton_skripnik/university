@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/news.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.news.store') !!}" enctype="multipart/form-data" method="post"
                  role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/news.create.form.fieldsets.title')</legend>

                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="control-label" for="news_title">
                        @lang('acp/news.create.form.fields.title.label')
                    </label>
                    <input required type="text" class="form-control"
                           name="title" id="news_title" value="{{ old('title') }}"
                           placeholder="@lang('acp/news.create.form.fields.title.label')">
                    @includeWhen($errors->has('title'), 'acp.component.field-error', ['field' => 'title'])
                </div>

                <div class="form-group required {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="control-label" for="news_description">
                        @lang('acp/news.create.form.fields.description.label')
                    </label>
                    <textarea rows="5" class="form-control editor markdown_editor"
                              name="description" id="news_description_editor"
                              placeholder="@lang('acp/news.create.form.fields.description.label')"
                    >{{ old('description') }}</textarea>
                    @includeWhen($errors->has('description'), 'acp.component.field-error', ['field' => 'description'])
                </div>

                <div class="form-group {{ $errors->has('author') ? ' has-error' : '' }}">
                    <label class="control-label" for="news_author">
                        @lang('acp/news.create.form.fields.author.label')
                    </label>
                    <input type="text" class="form-control"
                           name="author" id="news_author" value="{{ old('text') }}"
                           placeholder="@lang('acp/news.create.form.fields.author.label')">
                    @includeWhen($errors->has('author'), 'acp.component.field-error', ['field' => 'author'])
                </div>

                <div class="form-group required {{ $errors->has('picture') ? ' has-error' : '' }}">
                    <label class="control-label" for="picture">
                        @lang('acp/news.create.form.fields.picture.label')
                    </label>
                    <input required type="file" multiple accept="image/jpeg,image/png" class="form-control"
                           name="picture[]" id="picture">
                    @includeWhen($errors->has('picture'), 'acp.component.field-error', ['field' => 'picture'])
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/news.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
