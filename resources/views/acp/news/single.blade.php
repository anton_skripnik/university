@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{$item->title}}</strong></h4>
        </div>
        <div class="panel-body">
            <div class="col-md-offset-1 col-md-10" data-provide="markdown-editable">
                {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($item->description) !!}
            </div>
            <div class="col-md-offset-9 col-md-2">{{$item->author}}</div>
            <div class="news_image_block col-md-12">
                @forelse($item->getMedia('news') as $pic)
                    <div><img src="{{$pic->getUrl('sm')}}" alt=""></div>
                @empty
                @endforelse
            </div>
        </div>
        <div class="panel-footer text-right">
            <a href="{!! route('acp.news.edit', $item->slug) !!}" class="btn btn-primary">
                Редагувати
                <i class="fa fa-pencil"></i>
            </a>
        </div>
    </div>
@endsection
