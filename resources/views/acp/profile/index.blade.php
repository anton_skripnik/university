@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/profile.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.profile.update', \Auth::id()) !!}" method="post" role="form"
                  class="col-sm-8 col-sm-offset-2">
                {!! csrf_field() !!}
                {!! method_field('patch') !!}
                <legend>@lang('acp/profile.edit.form.fieldsets.title')</legend>

                <div class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label" for="user_name">
                        @lang('acp/profile.edit.form.fields.name.label')
                    </label>
                    <input value="{{$user->name}}" required type="text" class="form-control" name="name"
                           id="user_name" placeholder="@lang('acp/profile.edit.form.fields.name.label')">
                    @includeWhen($errors->has('name'), 'acp.component.field-error', ['field' => 'name'])
                </div>

                <div class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label" for="user_email">
                        @lang('acp/profile.edit.form.fields.email.label')
                    </label>
                    <input value="{{$user->email}}" required type="email" class="form-control" name="email"
                           id="user_email" placeholder="@lang('acp/profile.edit.form.fields.email.label')">
                    @includeWhen($errors->has('email'), 'acp.component.field-error', ['field' => 'email'])
                </div>

                <legend></legend>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">
                        @lang('acp/profile.edit.form.fields.password.label')
                    </label>
                    <input id="password" type="password" class="form-control" name="password">
                        @includeWhen($errors->has('password'), 'acp.component.field-error', ['field' => 'password'])
                </div>

                <div class="form-group">
                    <label for="password_confirmation" class="control-label">
                        @lang('acp/profile.edit.form.fields.password_confirmation.label')
                    </label>
                        <input id="password_confirmation" type="password"
                               class="form-control" name="password_confirmation">
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/profile.edit.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
