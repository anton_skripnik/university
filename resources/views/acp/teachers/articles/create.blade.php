@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel subject-create">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{$teacher->FullName}}</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.teacher.article.store', $teacher->slug) !!}" method="post"
                  role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/article.create.form.fieldsets.title')</legend>

                {!! csrf_field() !!}

                <publication inline-template>
                    <fieldset>
                        <div v-for="(row,index) in rows"
                             class="form-group {{ $errors->has('article') ? ' has-error' : '' }}">
                            <div v-if="index > 0" class="line col-sm-12"></div>
                            <div class="article-field col-md-11">
                                <label class="">@lang('acp/article.create.form.fields.article.label')</label>
                                <textarea rows="4" class="form-control" :name="row.article.n"
                                          v-model="row.article.v" id="subject"
                                          placeholder="@lang('acp/article.create.form.fields.article.label')">
                                </textarea>
                                @if ($errors->has('article'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('article') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="control-panel col-md-1 col-sm-1">
                                <button type="button" v-show="index > 0" class="btn btn-link">
                                    <span class="fa fa-trash-o" @click="del(row)"></span>
                                </button>
                                <button type="button" v-show="index < 1" class="btn btn-link" @click="add">
                                    <span class="fa fa-plus-square-o"></span>
                                </button>
                            </div>

                            <div class="link-field col-md-11">
                                <label class="">@lang('acp/article.create.form.fields.link.label')</label>
                                <input class="form-control" :name="row.link.n" v-model="row.link.v"
                                       id="subject" placeholder="@lang('acp/article.create.form.fields.link.label')">
                                @if ($errors->has('link'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('link') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="col-md-1"></div>

                        </div>
                    </fieldset>
                </publication>
                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/article.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection