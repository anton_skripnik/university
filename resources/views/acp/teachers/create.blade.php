@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/teacher.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.teacher.store') !!}" method="post" enctype="multipart/form-data"
                  role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/teacher.create.form.fieldsets.title')</legend>

                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label class="control-label" for="last_name">
                        @lang('acp/teacher.create.form.fields.last_name.label')
                    </label>
                    <input required type="text" class="form-control"
                           name="last_name" id="last_name" value="{{ old('last_name') }}"
                           placeholder="@lang('acp/teacher.create.form.fields.last_name.label')">
                    @includeWhen($errors->has('last_name'), 'acp.component.field-error', ['field' => 'last_name'])
                </div>

                <div class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label" for="name">@lang('acp/teacher.create.form.fields.name.label')</label>
                    <input required type="text" class="form-control"
                           name="name" id="name" value="{{ old('name') }}"
                           placeholder="@lang('acp/teacher.create.form.fields.name.label')">
                    @includeWhen($errors->has('name'), 'acp.component.field-error', ['field' => 'name'])
                </div>

                <div class="form-group required {{ $errors->has('post') ? ' has-error' : '' }}">
                    <label class="control-label" for="post">@lang('acp/teacher.create.form.fields.post.label')</label>
                    <input required type="text" class="form-control"
                           name="post" id="post" value="{{ old('post') }}"
                           placeholder="@lang('acp/teacher.create.form.fields.post.label')">
                    @includeWhen($errors->has('post'), 'acp.component.field-error', ['field' => 'post'])
                </div>

                <div class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label" for="email">@lang('acp/teacher.create.form.fields.email.label')</label>
                    <input required type="text" class="form-control"
                           name="email" id="email" value="{{ old('email') }}"
                           placeholder="@lang('acp/teacher.create.form.fields.email.label')">
                    @includeWhen($errors->has('email'), 'acp.component.field-error', ['field' => 'email'])
                </div>

                <div class="form-group required {{ $errors->has('schedule') ? ' has-error' : '' }}">
                    <label class="control-label" for="schedule">
                        @lang('acp/teacher.create.form.fields.schedule.label')
                    </label>
                    <input required type="text" class="form-control"
                           name="schedule" id="schedule" value="{{ old('schedule') }}"
                           placeholder="@lang('acp/teacher.create.form.fields.schedule.label')">
                    @includeWhen($errors->has('schedule'), 'acp.component.field-error', ['field' => 'schedule'])
                </div>

                <div class="form-group required {{ $errors->has('biography') ? ' has-error' : '' }}">
                    <label class="control-label" for="biography">
                        @lang('acp/teacher.create.form.fields.biography.label')
                    </label>
                    <textarea class="form-control markdown_editor"
                              name="biography" id="biography">{{ old('biography') }}</textarea>
                    @includeWhen($errors->has('biography'), 'acp.component.field-error', ['field' => 'biography'])
                </div>

                <div class="form-group required {{ $errors->has('avatar') ? ' has-error' : '' }}">
                    <label class="control-label" for="avatar">
                        @lang('acp/teacher.create.form.fields.avatar.label')
                    </label>
                    <input type="file" accept="image/jpeg,image/png" class="form-control" name="avatar" id="avatar">
                    @includeWhen($errors->has('avatar'), 'acp.component.field-error', ['field' => 'avatar'])
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/teacher.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
