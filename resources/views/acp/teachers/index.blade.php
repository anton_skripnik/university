@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/teacher.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>@lang('acp/teacher.main.table.name')</th>
                    <th class="text-center">@lang('acp/teacher.main.table.edit')</th>
                    <th class="text-center">@lang('acp/teacher.main.table.delete')</th>
                    <th class="text-center">@lang('acp/teacher.main.table.move')</th>
                </tr>
                </thead>
                <tbody id="teacher-sort">
                @forelse($teachers as $teacher)
                    <tr id="teacher_{{ $teacher->getKey()}}" data-id="{{$teacher->getKey() }}">
                        <td>
                            <a href="{!! route('acp.teacher.show', $teacher->slug) !!}">
                                {{ $teacher->FullName }}
                            </a>
                        </td>
                        <td class="text-center"><a href="{!! route('acp.teacher.edit', $teacher->slug) !!}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <delete url="{!! route('acp.teacher.destroy', $teacher->slug) !!}"
                                                        remove="#teacher_{{ $teacher->getKey() }}"></delete>
                        </td>
                        <td class="text-center">
                            <a href="#">
                                <i class="fa fa-arrows handle" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="4">Немає викладачів</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $teachers->links() }}
        </div>
    </div>
@endsection
