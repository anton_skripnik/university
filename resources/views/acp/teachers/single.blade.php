@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel single-teacher-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{ $teacher->FullName }}</strong></h4>
            <div>
                <a href="{!! route('acp.teacher.article.create', $teacher->slug) !!}" type="button"
                   class="btn btn-primary">@lang('acp/teacher.single-page.buttons.article.text')</a>
                <a href="{!! route('acp.teacher.subject.create', $teacher->slug) !!}" type="button"
                   class="btn btn-primary">@lang('acp/teacher.single-page.buttons.subject.text')</a>
                <a href="{!! route('acp.teacher.edit', $teacher->slug) !!}" type="button"
                   class="btn btn-primary">@lang('acp/teacher.single-page.buttons.edit.text')</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="single-teacher">
                <div>
                    <img src="{{ $teacher->photoUrl }}">
                </div>
                <div>
                    <p>{{ $teacher->post }}</p>
                    <p>{{ $teacher->email }}</p>
                    <p>{{ $teacher->schedule }}</p>

                    <!-- TAB NAVIGATION -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active">
                            <a href="#biography" role="tab"
                                              data-toggle="tab">
                                @lang('acp/teacher.single-page.tabs.biography')
                            </a>
                        </li>
                        <li><a href="#articles" role="tab"
                               data-toggle="tab">@lang('acp/teacher.single-page.tabs.articles')</a></li>
                        <li><a href="#subjects" role="tab"
                               data-toggle="tab">@lang('acp/teacher.single-page.tabs.subjects')</a></li>
                    </ul>
                    <!-- TAB CONTENT -->
                    <div class="tab-content">
                        <div class="active tab-pane fade in" id="biography">
                            {{ $teacher->biography }}
                        </div>

                        <div class="tab-pane fade" id="articles">
                            <publication teacher="{{ $teacher->slug }}" inline-template>
                                <div>
                                    <ul id="article-sort" class="list-group">
                                        <li :id="row.id" v-for="row in rows" class="list-group-item">
                                            <div :id="'article-block_'+row.id" class="article-block">
                                                <a class="" :href="row.link.v" target="_blank">
                                                    <span class="" v-text="row.article.v"></span>
                                                </a>
                                                <div class="control-block">
                                                    <a href="" @click.prevent="edit(row.id)">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="" @click.prevent="destroy(row.id)">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    <a v-if="page.total < page.per_page" href="" @click.prevent="">
                                                        <i class="fa fa-arrows handle" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div :id="'edit-article-block_'+row.id" class="edit-article-block">
                                                <div>
                                                        <textarea rows="4" type="text" required
                                                                  v-model="row.article.v"
                                                                  :name="row.article.n"
                                                                  class="form-control"></textarea>
                                                </div>
                                                <div class="input-group">
                                                        <input type="text" required
                                                               v-model="row.link.v"
                                                               :name="row.link.n"
                                                               class="form-control">
                                                        <span class="input-group-btn">
                                                            <button
                                                                    @click.prevent="update(row.article.v, row.link.v, row.id)"
                                                                    type="button"
                                                                    class="btn btn-default">
                                                                <i class="fa fa-check"></i>
                                                            </button>
                                                        </span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div v-if="page.total > page.per_page" class="pagination-group col-md-5">
                                        <pagination
                                                :current="page.current_page"
                                                :total="page.total"
                                                :per-page="page.per_page"
                                                @page-changed="fetchData"
                                        >
                                        </pagination>
                                        <div class="pagination-group__all-btn">
                                            <button @click.prevent="getAll" type="button" class="btn btn-primary">
                                                Усе
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </publication>
                        </div>

                        @php
                            $subjectRows = [];
                            $subjects = $teacher->subjects->sortBy('position')->values();

                            foreach ($subjects as $i => $subject) {
                                $item = ($subject instanceof App\Models\Teacher\Subject) ? $subject : $subject[$i];
                                $subjectRows[$i] = [
                                    'id' => $item->id,
                                    'name'    => ['n' => 'subject['.$i.']', 'v' => $item->name],
                                ];
                            }
                        @endphp

                        <div class="tab-pane fade" id="subjects">
                            <subject data="{{ json_encode($subjectRows) }}" inline-template>
                                <ul id="subject-sort" class="list-group">
                                    <li :id="row.id" v-for="row in rows" class="list-group-item">
                                        <div :id="'subject-block_'+row.id" class="subject-block">
                                            <span v-text="row.name.v"></span>
                                            <div class="control-block">
                                                <a href="" @click.prevent="edit(row.id)">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="" @click.prevent="destroy(row.id)">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <a href="" @click.prevent="">
                                                    <i class="fa fa-arrows handle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="input-group edit-subject-block" :id="'edit-subject-block_'+row.id">
                                            <input type="text" required v-model="row.name.v" :name="row.name.n"
                                                   class="form-control">
                                            <span class="input-group-btn">
                                                <button @click.prevent="update(row.name.v, row.id)" type="button"
                                                        class="btn btn-default">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </subject>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
