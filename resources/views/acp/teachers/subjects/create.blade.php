@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel subject-create">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{$teacher->FullName}}</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.teacher.subject.store', $teacher->slug) !!}" method="post"
                  role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/subject.create.form.fieldsets.title')</legend>

                {!! csrf_field() !!}

                <subject inline-template>
                    <fieldset>
                        <div v-for="(row,index) in rows"
                             class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                            <div class="col-md-11">
                                <input class="form-control" :name="row.name.n" v-model="row.name.v"
                                       id="subject" placeholder="@lang('acp/subject.create.form.fields.subject.label')">
                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('subject') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="col-md-1 col-sm-1">
                                <button type="button" v-show="index > 0" class="btn btn-link">
                                    <span class="fa fa-trash-o" @click="del(row)"></span>
                                </button>
                                <button type="button" v-show="index < 1" class="btn btn-link" @click="add">
                                    <span class="fa fa-plus-square-o"></span>
                                </button>
                            </div>

                        </div>
                    </fieldset>
                </subject>
                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/subject.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection