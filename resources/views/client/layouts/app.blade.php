<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('client.layouts.partial.head')
</head>
<body>
    <div id="app">
        <div id="wrapper">
            <div id="main">
                <div class="inner">
                    @includeIf('client.layouts.partial.header')
                    @yield('content')
                </div>
            </div>
            @include('client.layouts.partial.sidebar')
        </div>
    </div>
    @stack('scripts')
</body>
</html>
