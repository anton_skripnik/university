<header id="header">
    <strong>@lang('client/index.header.left')</strong>
    <span>@lang('client/index.header.right')</span>
</header>