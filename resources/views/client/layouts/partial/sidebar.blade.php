<div id="sidebar" class="inactive">
    <div class="inner">

        <!-- Menu -->
        <nav id="menu">
            <header class="major">
                <h2>@lang('client/index.sidebar.menu.label')</h2>
            </header>
            <ul>
                <li><a @if(app('request')->is('/')) class="active-link" @endif href="{{route('index')}}">@lang('client/index.sidebar.menu.main')</a></li>
                <li><a @if(app('request')->is('about*')) class="active-link" @endif href="{{route('about')}}">@lang('client/index.sidebar.menu.about')</a></li>
                <li><a @if(app('request')->is('gallery*')) class="active-link" @endif href="{{route('gallery')}}">@lang('client/index.sidebar.menu.gallery')</a></li>
                <li><a @if(app('request')->is('teachers*')) class="active-link" @endif href="{{route('teachers')}}">@lang('client/index.sidebar.menu.teachers')</a></li>
                <li><a @if(app('request')->is('graduates*')) class="active-link" @endif href="{{route('graduates')}}">@lang('client/index.sidebar.menu.graduates')</a></li>
                <li><a @if(app('request')->is('newcomers*')) class="active-link" @endif href="{{route('newcomers')}}">@lang('client/index.sidebar.menu.newcomers')</a></li>
                <li><a @if(app('request')->is('links*')) class="active-link" @endif href="{{route('links')}}">@lang('client/index.sidebar.menu.links')</a></li>
                <li><a @if(app('request')->is('contacts*')) class="active-link" @endif href="{{route('contact')}}">@lang('client/index.sidebar.menu.contacts')</a></li>
            </ul>
        </nav>

        <!-- Footer -->
        <footer id="footer">
            <p class="copyright">@lang('client/index.sidebar.footer')</p>
        </footer>

    </div>
</div>
