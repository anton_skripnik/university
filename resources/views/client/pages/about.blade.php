@extends('client.layouts.app')
@section('content')
    @if($item)
        <section id="banner" class="about_us_page">
            <div class="content about_block_content">
                <header class="major">
                    <h2>{{ $item->title }}</h2>
                </header>
                <div class="image_block">
                    <img src="{{ $item->getFirstMediaUrl('aboutus', 'lg') }}" alt="" />
                </div>
                <div class="main_info_block">
                    <div class="small-block">
                        {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml(
                        $item->description) !!}
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection
