@extends('client.layouts.app')
@section('content')
    @if($contacts)
        <section id="banner" class="contact_block">
            <header class="major">
                <h2>@lang('client/index.contacts.label')</h2>
            </header>
            <div class="block">
                <div id="map"></div>
                <div class="content">
                    <h2 class="news_header address">
                        <i class="fa fa-home icon"></i>
                        <div>
                            {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml(
                            $contacts->address) !!}
                        </div>
                    </h2>
                    @if($contacts->phone)
                        <h2 class="news_header"><i class="fa fa-phone icon"></i>{{ $contacts->phone }}</h2>
                    @endif
                </div>
            </div>
        </section>
    @endif
@endsection
@push('scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjwEwgEV035Ztf_-zIpRR4KokHNu_2cds&callback=initMap&language=uk">
    </script>
    <script>
        function initMap() {
            let uluru = {lat: 47.820240, lng: 35.182141};
            let map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: uluru
            });
            let marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
@endpush