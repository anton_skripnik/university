@extends('client.layouts.app')
@section('content')
<!-- Section -->
<section>
    <div id="gallery">
        @forelse($gallery as $item)
            <div class="gallery_image_block">
                <div class="image">
                    <a href="{{ $item->getMedia('gallery')->first()->getUrl() }}" data-lightbox="roadtrip"
                       data-title="{{ $item->title }}">
                        <img src="{{ $item->getPhotoUrlAttribute('md') }}" alt="">
                    </a>
                </div>
                <p>{{ $item->title }}</p>
            </div>
        @empty
        @endforelse
    </div>
    {{ $gallery->links() }}
</section>
@endsection