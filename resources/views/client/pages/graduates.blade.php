@extends('client.layouts.app')
@section('content')
    <!-- Section -->
    <section>
        <header class="major">
            <h2>@lang('client/index.graduates.label')</h2>
        </header>
        <div id="graduates">
            @forelse($graduates as $item)
                <article class="max-w block @if($loop->iteration%2 == 0) reverse @else original @endif">
                    <div class="header_block">
                        <h3>{{ $item->name }}</h3>
                        <p>{{ $item->subheader }}</p>
                    </div>
                    <div class="main_info_block">
                        <div class="image image_block">
                            <img src="{{ $item->getPhotoUrlAttribute('thumb') }}">
                        </div>
                        <div>
                            {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($item->text) !!}
                        </div>
                    </div>
                </article>
            @empty
            @endforelse
        </div>
    </section>
@endsection
