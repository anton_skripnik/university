@extends('client.layouts.app')
@section('content')
    @if($history)
        <section id="banner">
            <div class="content">
                <header>
                    <h1>{{ $history->title }}</h1>
                </header>
                <p>{{str_limit($history->description, 300)}}</p>
                <ul class="actions">
                    <li>
                        <a href="{{ route('about') }}" class="button big">
                            @lang('client/index.articles.btn.more_info')
                        </a>
                    </li>
                </ul>
            </div>
            <span class="image object">
                <img src="{{ $history->getFirstMediaUrl('aboutus','lg') }}" alt="" />
            </span>
        </section>
        <section id="news_section">
            <header class="major">
                <h2>@lang('client/index.articles.news.label')</h2>
            </header>
            <news inline-template>
                <div>
                    <div class="posts">
                        <article v-for="item in news">
                            <a href="#" @click.prevent="goToPage(item.slug)" class="image">
                                <img :src="item.image" alt="" />
                            </a>
                            <div class="other_block">
                                <h3 v-text="item.title"></h3>
                                <p v-text="item.description"></p>
                            </div>
                            <ul class="actions">
                                <li>
                                    <a href="" @click.prevent="goToPage(item.slug)"
                                       class="button">@lang('client/index.articles.btn.more_info')
                                    </a>
                                </li>
                            </ul>
                        </article>
                    </div>
                    <ul v-if="news.length < page.total" class="actions more_btn">
                        <li title="@lang('client/index.articles.btn.load_more')">
                            <svg @click.prevent="fetchData(page.current_page+1)" id="more-arrows">
                                <polygon class="arrow-top" points="37.6,27.9 1.8,1.3 3.3,0 37.6,25.3 71.9,0 73.7,1.3 "/>
                                <polygon class="arrow-middle" points="37.6,45.8 0.8,18.7 4.4,16.4 37.6,41.2 71.2,16.4 74.5,18.7 "/>
                                <polygon class="arrow-bottom" points="37.6,64 0,36.1 5.1,32.8 37.6,56.8 70.4,32.8 75.5,36.1 "/>
                            </svg>
                        </li>
                    </ul>
                </div>
            </news>
        </section>
    @endif
@endsection
