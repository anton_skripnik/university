@extends('client.layouts.app')
@section('content')
    <!-- Section -->
    <section id="info_section">
        <header class="major">
            <h2>@lang('client/index.links.label')</h2>
        </header>
        <div class="main-info">
            @foreach($categories as $category)
                <div class="main-info__block">
                    <a href="{{ route('links.show', $category->getKey()) }}">
                        <i class="fa fa-{{ $category->getAttribute('icon') }}"></i>
                        <p>{{ $category->getAttribute('category') }}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </section>
@endsection