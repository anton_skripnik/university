@extends('client.layouts.app')
@section('content')
    <!-- Section -->
    <section id="info_single_section">
        <header class="major">
            <h2>{{ $category }}</h2>
        </header>
        <div class="main-info">
            @foreach($info as $item)
                <div class="main-info__block">
                    <h4>{{ $item->getAttribute('title') }}
                        <small>{{ $item->getAttribute('created_at')->format('H:i / d.m.Y') }}</small></h4>
                    <div>{!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($item->description) !!}</div>
                    <div class="image-block">
                        @foreach($item->getMedia('info_pic') as $pic)
                            <div class="image">
                                <a target="_blank" href="{{ $pic->getUrl() }}"
                                   title="@lang('client/index.links.image_title')">
                                    <img src="{{ $pic->getUrl('sm') }}" alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="file-block">
                        @foreach($item->getMedia('info_file') as $file)
                            <div class="file">
                                <a target="_blank" href="{{ $file->getUrl() }}" title="@lang('client/index.links.file_title')">
                                    <p>
                                        <i class="fa fa-file fa-3x"></i>
                                        {{ $file->getAttribute('file_name') }}
                                    </p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
            {{ $info->links() }}
        </div>
    </section>
@endsection
