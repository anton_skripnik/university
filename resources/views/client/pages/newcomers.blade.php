@extends('client.layouts.app')
@section('content')
    <!-- Section -->
    <section>
      <header class="major">
        <h2>@lang('client/index.newcomers.label')</h2>
      </header>
      <newcomers>
      </newcomers>
    </section>
@endsection
