@extends('client.layouts.app')
@section('content')
    <!-- Banner -->
    <section id="banner" class="gallery-plugin">
        <div class="content single_news_content">
            <h2 class="news_header">{{ $item->title }}</h2>
            <p class="news_date">{{ $item->created_at->format('H:i / d.m.Y') }}</p>
            <div class="image_block">
                <img class="image" src="{{ $item->getFirstMediaUrl('news', 'lg') }}" alt="" />
            </div>
            <div class="main_info_block">
                <div class="small-block">
                    {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($item->description) !!}
                    <p class="author_name"><b>{{ $item->author }}</b></p>
                </div>
            </div>
            @if(count($item->getMedia('news')) > 1)
                <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                        @foreach($item->getMedia('news') as $pic)
                            <div class="swiper-slide" style="background-image:url({{ $pic->getUrl('sm') }})"></div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        @foreach($item->getMedia('news') as $pic)
                            <div class="swiper-slide" style="background-image:url({{ $pic->getUrl('lg') }})"></div>
                        @endforeach
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>
            @endif
        </div>
    </section>

    <!-- Section -->
    @if(count($news) > 0)
        <section id="other_news">
            <header class="major">
                <h2>@lang('client/index.articles.news.other_news')</h2>
            </header>
            <div class="posts">
                @forelse($news as $other)
                    <article>
                        <a href="#" class="image"><img src="{{ $other->getFirstMediaUrl('news', 'md') }}" alt="" /></a>
                        <div class="other_block">
                            <h3>{{ $other->title }}</h3>
                            <p>{{ str_limit($other->description, 100) }}</p>
                        </div>

                        <ul class="actions">
                            <li>
                                <a href="{{ route('news.show', $other->slug) }}" class="button">
                                    @lang('client/index.articles.btn.more_info')
                                </a>
                            </li>
                        </ul>
                    </article>
                @empty
                @endforelse
            </div>
        </section>
    @endif
@endsection
