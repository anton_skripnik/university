@extends('client.layouts.app')
@section('content')

    <!-- Section -->
    <section>
        <div id="single_teacher" class="align-items-center">
            <div>
                <div class="main_block max-w">
                    <div class="image image_block">
                        <img src="{{ $teacher->getPhotoUrlAttribute('thumb') }}">
                    </div>
                    <div class="main_info_block">
                        <div>
                            <h2 class="header_block">{{ $teacher->FullName }}</h2>
                            <p><b>{{ $teacher->post }}</b></p>
                            <p><b>@lang('client/index.teachers.email')</b>{{ $teacher->email }}</p>
                            <p><b>@lang('client/index.teachers.schedule')</b>{{ $teacher->schedule }}</p>
                        </div>

                        <!-- TAB NAVIGATION -->

                        <ul class="nav nav-tabs tabs_block actions" role="tablist">
                            <li class="active">
                                <a href="#biography" role="tab" data-toggle="tab" class="button">
                                    @lang('client/index.teachers.biography')
                                </a>
                            </li>
                            <li><a href="#articles" role="tab"
                                   data-toggle="tab" class="button">@lang('client/index.teachers.articles')</a></li>
                            <li><a href="#subjects" role="tab"
                                   data-toggle="tab" class="button">@lang('client/index.teachers.subjects')</a></li>
                        </ul>
                    </div>
                </div>
                <!-- TAB CONTENT -->
                <div class="tab-content content_block max-w">
                    <div class="active tab-pane fade in" id="biography">
                        {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($teacher->biography) !!}
                    </div>

                    <div class="tab-pane fade" id="articles">
                        <div class="list-group">
                            @forelse($teacher->articles as $item)
                                @if(\blank($item->link))
                                    <a href="#" class="list-group-item inactive">
                                        {{ $item->description }}
                                    </a>
                                @else
                                    <a href="{{ $item->link }}" target="_blank"
                                       class="list-group-item">
                                        {{ $item->description }}
                                    </a>
                                @endif
                            @empty
                            @endforelse
                        </div>
                    </div>

                    <div class="tab-pane fade" id="subjects">
                        <div class="list-group">
                            @forelse($teacher->subjects as $item)
                                <a href="#" class="list-group-item inactive">{{ $item->name }}</a>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
