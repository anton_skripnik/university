@extends('client.layouts.app')
@section('content')
    <section>
        <header class="major">
            <h2>@lang('client/index.teachers.label')</h2>
        </header>
        <div id="teachers">
            @forelse($teachers as $item)
                <article class="block">
                    <div>
                        <a href="#" class="image"><img src="{{ $item->getPhotoUrlAttribute('thumb') }}" alt="" /></a>
                        <a class="single_link" href="{{ route('teacher.show', $item->slug) }}">
                            <h3 class="teachers_name"><span>{{ $item->last_name }}</span> <br> {{ $item->name }}</h3>
                            <p class="teachers_post">{{ $item->post }}</p>
                        </a>
                    </div>
                </article>
            @empty
            @endforelse
        </div>
    </section>
@endsection