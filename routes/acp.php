<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| ACP Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "acp" middleware group.
|
*/

Route::get('/', function () {
    return \redirect()->route('acp.news.index');
})->name('.index');

Route::group([
    'as'     => '.news',
    'prefix' => 'news',
], function () {
    require_once(base_path('routes/acp/news.php'));
});

Route::group([
    'as'     => '.profile',
    'prefix' => 'profile',
], function () {
    require_once(base_path('routes/acp/profile.php'));
});

Route::group([
    'as'        => '.teacher',
    'prefix'    => 'teachers',
    'namespace' => 'Teacher',
], function () {
    require_once(base_path('routes/acp/teacher.php'));
});

Route::group([
    'as'        => '.link',
    'prefix'    => 'links',
    'namespace' => 'Link',
], function () {
    require_once(base_path('routes/acp/link.php'));
});

Route::group([
    'as'        => '.history',
    'prefix'    => 'history',
], function () {
    require_once(base_path('routes/acp/history.php'));
});

Route::group([
    'as'        => '.gallery',
    'prefix'    => 'gallery',
], function () {
    require_once(base_path('routes/acp/gallery.php'));
});

Route::group([
    'as'        => '.contact',
    'prefix'    => 'contacts',
], function () {
    require_once(base_path('routes/acp/contact.php'));
});

Route::group([
    'as'        => '.graduate',
    'prefix'    => 'graduates',
], function () {
    require_once(base_path('routes/acp/graduate.php'));
});

Route::group([
    'as'        => '.newcomerscontactinfo',
    'prefix'    => 'newcomerscontactinfo',
], function () {
    require_once(base_path('routes/acp/newcomerscontactinfo.php'));
});
