<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.show',
    'uses' => 'ContactController@show',
]);

Route::patch('/{contact}', [
    'as'   => '.update',
    'uses' => 'ContactController@update',
]);

