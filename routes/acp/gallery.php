<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.index',
    'uses' => 'GalleryController@index',
]);

Route::get('/create', [
    'as'   => '.create',
    'uses' => 'GalleryController@create',
]);

Route::post('/', [
    'as'   => '.store',
    'uses' => 'GalleryController@store',
]);

Route::get('/{photo}', [
    'as'   => '.show',
    'uses' => 'GalleryController@show',
]);

Route::get('/{photo}/edit', [
    'as'   => '.edit',
    'uses' => 'GalleryController@edit',
]);

Route::patch('/{photo}', [
    'as'   => '.update',
    'uses' => 'GalleryController@update',
]);

Route::delete('/{photo}', [
    'as'   => '.destroy',
    'uses' => 'GalleryController@destroy',
]);
