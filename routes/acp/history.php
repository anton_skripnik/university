<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.index',
    'uses' => 'HistoryController@index',
]);

Route::get('/create', [
    'as'         => '.create',
    'uses'       => 'HistoryController@create',
    'middleware' => 'history',
]);

Route::post('/', [
    'as'         => '.store',
    'uses'       => 'HistoryController@store',
    'middleware' => 'history',
]);

Route::get('/{history}', [
    'as'   => '.show',
    'uses' => 'HistoryController@show',
]);

Route::get('/{history}/edit', [
    'as'   => '.edit',
    'uses' => 'HistoryController@edit',
]);

Route::patch('/{history}', [
    'as'   => '.update',
    'uses' => 'HistoryController@update',
]);

Route::delete('/{history}', [
    'as'   => '.destroy',
    'uses' => 'HistoryController@destroy',
]);

Route::delete('image/{media}', [
    'as'   => '.pic.destroy',
    'uses' => 'HistoryController@imageDestroy',
]);
