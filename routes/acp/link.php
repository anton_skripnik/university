<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.index',
    'uses' => 'LinkController@index',
]);

Route::get('create', [
    'as'   => '.create',
    'uses' => 'LinkController@create',
]);

Route::post('/', [
    'as'   => '.store',
    'uses' => 'LinkController@store',
]);

Route::get('{link}/show', [
    'as'   => '.show',
    'uses' => 'LinkController@show',
]);

Route::get('{link}', [
    'as'   => '.show',
    'uses' => 'LinkController@show',
]);

Route::get('{link}/edit', [
    'as'   => '.edit',
    'uses' => 'LinkController@edit',
]);

Route::patch('{link}', [
    'as'   => '.update',
    'uses' => 'LinkController@update',
]);

Route::delete('{link}', [
    'as'   => '.destroy',
    'uses' => 'LinkController@destroy',
]);

Route::delete('image/{media}', [
    'as'   => '.pic.destroy',
    'uses' => 'LinkController@imageDestroy',
]);

//categories
Route::get('categories', [
    'as'   => '.category.show',
    'uses' => 'CategoryLinkController@show',
]);

Route::patch('categories/{category}', [
    'as'   => '.category.update',
    'uses' => 'CategoryLinkController@update',
    'laroute' => \true,
]);

Route::delete('categories/{category}', [
    'as'   => '.category.destroy',
    'uses' => 'CategoryLinkController@destroy',
    'laroute' => \true,
]);


