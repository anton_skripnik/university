<?php
declare(strict_types=1);

Route::get('/', [
    'as'    => '.index',
    'uses'  => 'NewcomersContactInfoController@index',
]);
?>
