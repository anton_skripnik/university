<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.index',
    'uses' => 'NewsController@index',
]);

Route::get('/create', [
    'as'   => '.create',
    'uses' => 'NewsController@create',
]);

Route::post('/', [
    'as'   => '.store',
    'uses' => 'NewsController@store',
]);

Route::get('/{news}', [
    'as'   => '.show',
    'uses' => 'NewsController@show',
]);

Route::get('/{news}/edit', [
    'as'   => '.edit',
    'uses' => 'NewsController@edit',
]);

Route::patch('/{news}', [
    'as'   => '.update',
    'uses' => 'NewsController@update',
]);

Route::delete('/{news}', [
    'as'   => '.destroy',
    'uses' => 'NewsController@destroy',
]);

Route::delete('image/{media}', [
    'as'   => '.pic.destroy',
    'uses' => 'NewsController@imageDestroy',
]);
