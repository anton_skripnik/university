<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.show',
    'uses' => 'ProfileController@show',
]);

Route::patch('/{user}', [
    'as'   => '.update',
    'uses' => 'ProfileController@update',
]);

