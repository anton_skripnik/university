<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use \App\Http\Requests\Acp\NewcomersContactInfoRequest;

//auth routes
Auth::routes();

Route::get('logout', [
    'as'         => '.logout',
    'uses'       => 'Auth\LoginController@logout',
    'middleware' => ['auth'],
]);

//client routes
Route::get('/', [
    'as'   => 'index',
    'uses' => 'HomeController@index',
]);

Route::get('news/paginate', [
    'as'      => 'news.showMore',
    'uses'    => 'HomeController@newsShowMore',
    'laroute' => \true,
]);

Route::get('news/{news}', [
    'as'   => 'news.show',
    'uses' => 'HomeController@newsShow',
    'laroute' => \true,
]);

Route::get('teachers/{teacher}', [
    'as'   => 'teacher.show',
    'uses' => 'HomeController@teacherShow',
]);

Route::get('teachers', [
    'as'   => 'teachers',
    'uses' => 'HomeController@teacherIndex',
]);

Route::get('graduates', [
    'as'   => 'graduates',
    'uses' => 'HomeController@graduateIndex',
]);

Route::get('newcomers', [
    'as'   => 'newcomers',
    'uses' => 'HomeController@newcomersIndex',
]);

Route::get('about', [
    'as'   => 'about',
    'uses' => 'HomeController@aboutIndex',
]);

Route::get('links', [
    'as'   => 'links',
    'uses' => 'HomeController@infoIndex',
]);

Route::get('links/{category}', [
    'as'   => 'links.show',
    'uses' => 'HomeController@infoShow',
]);

Route::get('gallery', [
    'as'      => 'gallery',
    'uses'    => 'HomeController@galleryIndex',
]);

Route::get('contacts', [
    'as'   => 'contact',
    'uses' => 'HomeController@contactIndex',
]);

Route::post('newcomers-contact-info', function(\App\Http\Requests\Acp\NewcomersContactInfoRequest $request) {
    $controller = new \App\Http\Controllers\Acp\NewcomersContactInfoController;
    $controller->store($request);

    return response("Success", 200);
});
