const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const WebpackShellPlugin = require('webpack-shell-plugin');
mix.webpackConfig({
    plugins:
            [
                new WebpackShellPlugin({
                    onBuildStart:[],
                    onBuildEnd:[
                        'php artisan laroute:generate'
                    ],
                })
            ],
});

mix
// generic
        //.copy('resources/assets/generic/images', 'public/build/generic/images', false)
        .copy('resources/assets/generic/font-awesome/fonts', 'public/build/generic/fonts', false)
        .options({processCssUrls: false})
        .sass('resources/assets/generic/font-awesome/scss/font-awesome.scss', 'public/build/generic/css')

        // client
        .copy('resources/assets/client/images', 'public/build/client/images', false)
        .copy('node_modules/lightbox2/dist/images', 'public/build/client/images', false)
        .sass('resources/assets/client/scss/app.scss', 'public/build/client/css/app-client.css', {
            processCssUrls: false
        })
        .js('resources/assets/client/js/app.js', 'public/build/client/js/app-client.js')

        // acp
        .copy('resources/assets/acp/images', 'public/build/acp/images', false)
        .options({processCssUrls: false})
        .sass('resources/assets/acp/scss/app.scss', 'public/build/acp/css/app-acp.css')
        .js('resources/assets/acp/js/app.js', 'public/build/acp/js/app-acp.js')

        // version
        .version([
            'public/build/client/js/app-client.js',
            'public/build/client/css/app-client.css',
            'public/build/acp/js/app-acp.js',
            'public/build/acp/css/app-acp.css'
        ]);
